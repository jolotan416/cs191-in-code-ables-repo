# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).

r1 = Role.create({name: "Students", description: "By default, student accounts"})
r2 = Role.create({name: "Admin", description: "Can perform any CRUD operation on any resource"})
r3 = Role.create({name: "Teacher", description: "Can do CRUB operation inside his/her own class"})
r4 = Role.create({name: "Employee", description: "Can upload documents for public view"})

u1 = User.create({username: "johncena", email: "cena@example.com", first_name: "John", middle_name: "Michael",
 last_name: "Cena", phone_number: "09152122343", suffix: "", valid_till: DateTime.new(2016,11,12,8,37,48,"+08:00"),
 password: "aaaaaaaa", password_confirmation: "aaaaaaaa", role_id: r1.id})

u2 = User.create({username: "jojobinay", email: "binay@example.com", first_name: "Jojo", middle_name: "Poe",
 last_name: "Binay", phone_number: "09152122343", suffix: "", valid_till: DateTime.new(2016,11,12,8,37,48,"+08:00"),
 password: "aaaaaaaa", password_confirmation: "aaaaaaaa", role_id: r1.id})

u3 = User.create({username: "rodyduterte", email: "duterte@example.com", first_name: "Rodrigo", middle_name: "Defensor",
 last_name: "Duterte", phone_number: "09152122343", suffix: "III", valid_till: DateTime.new(3000,11,12,8,37,48,"+08:00"),
  password: "aaaaaaaa", password_confirmation: "aaaaaaaa", role_id: r1.id})

u4 = User.create({username: "hackereduard", email: "hackereduard@example.com", first_name: "ADMIN", middle_name: "ADMIN",
 last_name: "ADMIN", phone_number: "00000000000", suffix: "", valid_till: DateTime.new(3000,11,12,8,37,48,"+08:00"),
 password: "aaaaaaaa", password_confirmation: "aaaaaaaa", role_id: r2.id})

u5 = User.create({username: "hackerjolo", email: "hackerjolo@example.com", first_name: "ADMIN", middle_name: "ADMIN",
 last_name: "ADMIN", phone_number: "00000000000", suffix: "", valid_till: DateTime.new(2010,11,12,8,37,48,"+08:00"),
 password: "aaaaaaaa", password_confirmation: "aaaaaaaa", role_id: r2.id})

u6 = User.create({username: "hackertopher", email: "hackertopher@example.com", first_name: "ADMIN", middle_name: "ADMIN",
 last_name: "ADMIN", phone_number: "00000000000", suffix: "I", valid_till: DateTime.new(3000,11,12,8,37,48,"+08:00"),
 password: "aaaaaaaa", password_confirmation: "aaaaaaaa", role_id: r2.id})

u7 = User.create({username: "hackerteacher", email: "hackerteacher@example.com", first_name: "Miriam", middle_name: "Defensor",
 last_name: "Santiago", phone_number: "00000000000", suffix: "I", valid_till: DateTime.new(3000,11,12,8,37,48,"+08:00"),
 password: "aaaaaaaa", password_confirmation: "aaaaaaaa", role_id: r3.id})

u8 = User.create({username: "hackerdimayuga", email: "hackerdiyamuga@example.com", first_name: "Sir John", middle_name: "Dibdib",
 last_name: "Dimayuga", phone_number: "00000000000", suffix: "I", valid_till: DateTime.new(3000,11,12,8,37,48,"+08:00"),
 password: "aaaaaaaa", password_confirmation: "aaaaaaaa", role_id: r3.id})

u9 = User.create({username: "hackeremployee", email: "hackeremployee@example.com", first_name: "Sir", middle_name: "Wilson",
 last_name: "Dragun", phone_number: "00000000000", suffix: "I", valid_till: DateTime.new(3000,11,12,8,37,48,"+08:00"),
 password: "aaaaaaaa", password_confirmation: "aaaaaaaa", role_id: r4.id})

u10 = User.create({username: "hackersusan", email: "hackersusan@example.com", first_name: "Susan", middle_name: "Felizmenio",
 last_name: "Edgardo", phone_number: "00000000000", suffix: "I", valid_till: DateTime.new(3000,11,12,8,37,48,"+08:00"),
 password: "aaaaaaaa", password_confirmation: "aaaaaaaa", role_id: r4.id})


tl1 = TeacherList.create({title: "Instructor I", user_id: u7.id})
tl2 = TeacherList.create({title: "PhD", user_id: u8.id})

el1 = EmployeeList.create({title: nil, user_id: u9.id, created_at: Time.now, updated_at: nil})
el2 = EmployeeList.create({title: nil, user_id: u10.id, created_at: Time.now, updated_at: nil})


d1 = Department.create({name: "Humanities", current_size: 40, capacity: 69})
d2 = Department.create({name: "Mathematics", current_size: 40, capacity: 69})
d3 = Department.create({name: "Physical Education", current_size: 40, capacity: 69})
d4 = Department.create({name: "Music", current_size: 40, capacity: 69})
d5 = Department.create({name: "Computer Science", current_size: 1, capacity: 1})

w1 = WorksIn.create({start_date: Time.now, end_date: nil, department_id: d1.id, user_id: u7.id, created_at: Time.now, updated_at: nil})
w2 = WorksIn.create({start_date: Time.now, end_date: nil, department_id: d2.id, user_id: u8.id, created_at: Time.now, updated_at: nil})
w3 = WorksIn.create({start_date: Time.now, end_date: nil, department_id: d3.id, user_id: u7.id, created_at: Time.now, updated_at: nil})

s1 = Subject.create({subject_name: "Math 17", offered: true, department_id: d2.id, created_at: Time.now, updated_at: Time.now})
s2 = Subject.create({subject_name: "PE 3", offered: true, department_id: d3.id, created_at: Time.now, updated_at: Time.now})
s3 = Subject.create({subject_name: "CS 11", offered: true, department_id: d4.id, created_at: Time.now, updated_at: Time.now})
s4 = Subject.create({subject_name: "CS 192", offered: true, department_id: d4.id, created_at: Time.now, updated_at: Time.now})

cc1 = ClassCourse.create({room: "Room 301", year: 2016, schedule: "MWF 11:00am - 12:00nn", size: 0, subject_id: s1.id, created_at: Time.now, updated_at: Time.now, name: "MTHRXWV"})
cc2 = ClassCourse.create({room: "Room 302", year: 2016, schedule: "MWF 12:00nn - 1:00pm", size: 0, subject_id: s2.id, created_at: Time.now, updated_at: Time.now, name: "MTHRABC"})
cc3 = ClassCourse.create({room: "Room 303", year: 2016, schedule: "MWF 10:00am - 1:00pm", size: 0, subject_id: s3.id, created_at: Time.now, updated_at: Time.now, name: "MTHRXYZ"})
cc4 = ClassCourse.create({room: "Room 304", year: 2016, schedule: "TTh 1:00pm - 8:00pm", size: 0, subject_id: s4.id, created_at: Time.now, updated_at: Time.now, name: "THU"})

cl1 = ClassList.create({user_id: u1.id, class_courses_id: cc1.id, status: "Enrolled", created_at: Time.now, updated_at: Time.now})
cl2 = ClassList.create({user_id: u2.id, class_courses_id: cc1.id, status: "Enrolled", created_at: Time.now, updated_at: Time.now})
cl3 = ClassList.create({user_id: u3.id, class_courses_id: cc1.id, status: "Enrolled", created_at: Time.now, updated_at: Time.now})
cl4 = ClassList.create({user_id: u1.id, class_courses_id: cc2.id, status: "Enrolled", created_at: Time.now, updated_at: Time.now})
cl5 = ClassList.create({user_id: u2.id, class_courses_id: cc2.id, status: "Enrolled", created_at: Time.now, updated_at: Time.now})
cl6 = ClassList.create({user_id: u1.id, class_courses_id: cc3.id, status: "Enrolled", created_at: Time.now, updated_at: Time.now})
