class CreateClassLists < ActiveRecord::Migration
  def change
    create_table :class_lists do |t|
      t.references :user, index: true, foreign_key: true
      t.references :class_courses, index: true, foreign_key: true
      t.string :status

      t.timestamps null: false
    end
  end
end
