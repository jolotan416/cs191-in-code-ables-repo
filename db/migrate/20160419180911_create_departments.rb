class CreateDepartments < ActiveRecord::Migration
  def change
    create_table :departments do |t|
      t.string :name
      t.integer :current_size
      t.integer :capacity

      t.timestamps null: false
    end
  end
end
