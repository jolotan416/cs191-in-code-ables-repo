class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :username,           unique: true
      t.string :email,              null: false, default: ""
      t.string :first_name
      t.string :middle_name
      t.string :last_name
      t.string :phone_number
      t.string :suffix
      t.datetime :valid_till
      t.belongs_to :role, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
