class CreateClassCourses < ActiveRecord::Migration
  def change
    create_table :class_courses do |t|
      t.string :room
      t.integer :year
      t.string :schedule
      t.integer :size
      t.belongs_to :subject, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
