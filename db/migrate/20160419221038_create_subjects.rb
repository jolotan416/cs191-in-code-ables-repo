class CreateSubjects < ActiveRecord::Migration
  def change
    create_table :subjects do |t|
      t.string :subject_name
      t.boolean :offered
      t.belongs_to :department, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
