$(window).load(function(){

	//$('#banner-left').hide();
	//$('#banner-right').hide();


  //Open Curtain
  	//auto open at 5 secs;
  	//setTimeout(hideBanner,5000);
	  $('.banner .continue').click(hideBanner);

  //SignIn
  	$('#content-sign-in').click(toggleSignIn);
  	$('.banner .sign_in').click(toggleSignIn);
  	$('#sign-in .cancel').click(toggleSignIn);

  //Navbar
  	$(window).scroll(function () {

    if ($(window).scrollTop() >= ($('header').outerHeight())) {
      $('nav').addClass('navbar-fixed');
    }
    else {
      $('nav').removeClass('navbar-fixed');
    }
  });

  //Carousel
  	$('#myCarousel').css('height',(window.innerHeight-($('header').outerHeight()+$('nav').outerHeight())).toString()+'px');

});
