$(function(){
  $('a.item').click(function(){
    $('.item').removeClass('active');
    $(this).addClass('active');
  })

  $('.field .ui.search.selection.dropdown').dropdown();
  $('.field .ui.selection.dropdown').dropdown();

  $('.ui.accordion').accordion({exclusive:false});

  $('.ui.modal').modal('attach events', '.showmodal');
});
