Roles
1.) Admin
2.) Moderator

User
username:string
email:string
first_name:string
middle_name:string
last_name:string
phone_number:string
suffix:string
valid_till:datetime
role:belongs_to


rails g scaffold User username:string email:string first_name:string middle_name:string last_name:string phone_number:string suffix:string valid_till:datetime role:belongs_to
rails g scaffold role name:string description:string


rails g scaffold item name:string description:text 'price:decimal{5,2}', user:belongs_to
rake db:migrate

=====
<!DOCTYPE html>
<html>
<head>
  <title><%= full_title(yield(:title))%></title>
  <%= stylesheet_link_tag    'application', media: 'all', 'data-turbolinks-track' => true %>
  <%= javascript_include_tag 'application', 'data-turbolinks-track' => true %>
  <%= csrf_meta_tags %>
</head>
<body>

<div class="ui top attached menu">
  <div class="ui dropdown icon item">
    <i class="wrench icon"></i>
    <div class="menu">
      <div class="item">
        <i class="dropdown icon"></i>
        <span class="text">New</span>
        <div class="menu">
          <div class="item">Document</div>
          <div class="item">Image</div>
        </div>
      </div>
      <div class="item">;loca
        Open...
      </div>
      <div class="item">
        Save...
      </div>
      <div class="item">Edit Permissions</div>
      <div class="divider"></div>
      <div class="header">
        Export
      </div>
      <div class="item">
        Share...
      </div>
    </div>
  </div>
  <div class="right menu">
    <div class="ui right aligned category search item">
      <div class="ui transparent icon input">
        <input class="prompt" type="text" placeholder="Search animals...">
        <i class="search link icon"></i>
      </div>
      <div class="results"></div>
    </div>
  </div>
</div>
<div class="ui bottom attached segment">
  <p></p>
</div>
<nav class="ui inverted blue secondary pointing demo menu" id="navbar">
  
  <h3 class="header item">
        <%= image_tag("logo.png", :class=>'ui small image', :id=>'header_logo') %>
  </h3>
  <a class="active item">Home</a>
  <a class="item">Classes</a>
  <a class="item">Clubs</a>
  <a class="item">Discussions</a>

  <div class="ui label">
  <i class="mail icon"></i> 23
  </div>
  <div class="ui floated right label" id="username">Welcome, Eduard Valdez!</div>
</nav>
<div class="ui medium image">
  <div class="ui corner label">
    <i class="help link icon" data-content="This appears to the right"></i>
  </div>
  <img src="/images/demo/photo.jpg" data-content="This appears in the default location">
</div>
<select name="gender" class="ui dropdown" id="select">
  <option value="">Gender</option>
  <option value="male">Male</option>
  <option value="female">Female</option>
</select>

<% if user_signed_in? %>
  Signed in as <%= current_user.username %>. Not you?
  <%= link_to "Edit profile", edit_user_registration_path %>
  <%= link_to "Sign out", destroy_user_session_path, :method => :delete %>
<% else %>
  <%= link_to "Sign up", new_user_registration_path %> or <%= link_to "sign in", new_user_session_path %>
<% end %>

<% flash.each do |name, msg| %>
  <%= content_tag :div, msg, id: "flash_#{name}" %>
<% end %>

<%= yield %>
<br>


<div class="ui grid">
        <div class="one wide row">
            <div class="ui green menu">
                <a class="active item">
                    <i class="home icon"></i> Home
                </a>
                <a class="item">
                    <i class="mail icon"></i> Messages
                </a>
                <div class="right menu">
                    <div class="item">
                        <div class="ui transparent icon input">
                            <input type="text" placeholder="Search...">
                            <i class="search link icon"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<div class="ui inverted vertical footer segment">
	<br>
	<br>
	<br>
	<br>
    <div class="ui center aligned container">
      <div class="ui stackable inverted divided grid">
        <div class="three wide column">
          <h4 class="ui inverted header">Group 1</h4>
          <div class="ui inverted link list">
            <a href="#" class="item">Link One</a>
            <a href="#" class="item">Link Two</a>
            <a href="#" class="item">Link Three</a>
            <a href="#" class="item">Link Four</a>
          </div>
        </div>
        <div class="three wide column">
          <h4 class="ui inverted header">Group 2</h4>
          <div class="ui inverted link list">
            <a href="#" class="item">Link One</a>
            <a href="#" class="item">Link Two</a>
            <a href="#" class="item">Link Three</a>
            <a href="#" class="item">Link Four</a>
          </div>
        </div>
        <div class="three wide column">
          <h4 class="ui inverted header">Group 3</h4>
          <div class="ui inverted link list">
            <a href="#" class="item">Link One</a>
            <a href="#" class="item">Link Two</a>
            <a href="#" class="item">Link Three</a>
            <a href="#" class="item">Link Four</a>
          </div>
        </div>
        <div class="seven wide column">
          <h4 class="ui inverted header">Footer Header</h4>
          <p>Extra space for a call to action inside the footer that could help re-engage users.</p>
        </div>
      </div>
      <div class="ui inverted section divider"></div>
      <%= image_tag("logo.png", :class=>'ui centered mini image') %>
      <div class="ui horizontal inverted small divided link list">
        <a class="item" href="#">Site Map</a>
        <a class="item" href="#">Contact Us</a>
        <a class="item" href="#">Terms and Conditions</a>
        <a class="item" href="#">Privacy Policy</a>
      </div>
    </div>
  </div>

<script>
 $('.ui.green.menu')
    .on('click', '.item', function() {
      if(!$(this).hasClass('dropdown')) {
        $(this)
          .addClass('active')
          .siblings('.item')
          .removeClass('active');
      }
    });
  $('.ui.dropdown').dropdown()
   $('.ui.inverted.blue.secondary.pointing.demo.menu')
      .on('click', '.item', function() {
        if(!$(this).hasClass('dropdown')) {
          $(this)
            .addClass('active')
            .siblings('.item')
              .removeClass('active');
        }
      });

$('#select')
  .dropdown()
;
// just initializing
$('.ui.image img')
  .popup()
;
// initializing with settings
$('.ui.image .help')
  .popup({
    position: 'right center'
  })
;
</script>
</body>
</html>
