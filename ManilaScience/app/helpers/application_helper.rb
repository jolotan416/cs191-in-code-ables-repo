module ApplicationHelper
	def full_title(page_title='')
		base_title = "Manila Science High School Website"
		if page_title.empty?
			"Manila Science High School Website"
		else
			page_title + " | " + base_title
		end
	end
end
