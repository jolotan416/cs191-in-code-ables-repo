class ClassViewController < ApplicationController
	def index
		@calendar_events = CalendarEvent.all

		# This is the ClassList Model
		if Role.find(current_user.role_id).name == "Students"
			@classes_temp = ClassList.where(:user_id => current_user.id)
		else
			@classes_temp = ClassCourse.where(:teacher_id => current_user.id)
		end
		# 
		for i in (0...@classes_temp.length)
			@classes_temp[i] = @classes_temp[i].attributes
			if Role.find(current_user.role_id).name != "Students"
				@classes_temp[i]["class_course_id"] = @classes_temp[i]["id"]
			end
		end

		# Now find
		# @classes_temp_courses = ClassList.where(:user_id => current_user.id)
		# 

		# We need to find what is the user's section and class adviser
		@section = ""
		@adviser = ""

		for i in (0...@classes_temp.length)
			# We are now joining the ClassList Model to the Course Model
			@course = ClassCourse.find(@classes_temp[i]["class_course_id"])
			# 
			@classes_temp[i]["room"] = @course.room
			@classes_temp[i]["year"] = @course.year
			@classes_temp[i]["schedule"] = @course.schedule
			@classes_temp[i]["size"] = @course.size
			@classes_temp[i]["subject_id"] = @course.subject_id
			# @classes_temp[i]["created_at"] = @course.created_at
			# @classes_temp[i]["updated_at"] = @course.updated_at
			@classes_temp[i]["name"] = @course.name
			@classes_temp[i]["teacher_id"] = @course.teacher_id
			@classes_temp[i]["is_club"] = @course.is_club

			# We are now joining the ClassList Model to the Course Model to the Subject
			@subject = Subject.find(@classes_temp[i]["subject_id"])
			@classes_temp[i]["subject_name"] = @subject.subject_name
			@classes_temp[i]["offered"] = @subject.offered
			@classes_temp[i]["department_id"] = @subject.department_id

			# @classes_temp[i]["subj_created_at"] = @subject.created_at
			# @classes_temp[i]["subj_updated_at"] = @subject.created_at
			# 


			if( @classes_temp[i]["subject_name"] == "Advisory Class" )
				@section = @classes_temp[i]["name"]
				@adviser = User.find(@classes_temp[i]["teacher_id"]).first_name + " " + User.find(@classes_temp[i]["teacher_id"]).last_name
			end
		end
		# 

		@classes = Array.new
		for i in (0...@classes_temp.length)
			if @classes_temp[i]["is_club"] == false
				@classes.append(@classes_temp[i])
			end
		end
	end

	def view_class
		@id = params[:class_course_id]
		@class_course = ClassCourse.find(@id)
    @subject = Subject.all
    @users = User.all
    @classes_lists = ClassList.where(:class_course_id => @id)
    @posts = Post.where(:class_course_id => @id)

		#
	end
end
