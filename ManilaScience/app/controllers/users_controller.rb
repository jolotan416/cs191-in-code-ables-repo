class UsersController < ApplicationController
  before_filter :authenticate_user!
  load_and_authorize_resource

  # GET /users
  # GET /users.json
  def index
    @users = User.all
    @roles = Role.all
    @highest = @roles.length
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @joined_on = @user.created_at.to_formatted_s(:short)
    if @user.current_sign_in_at
      @last_login = @user.current_sign_in_at.to_formatted_s(:short)
    else
      @last_login = "never"
    end
  end

  # GET /users/new
  def new
    #
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)
    # YEAR, MONTH, DATE
    original_username = params[:user][:first_name][0].downcase + params[:user][:middle_name][0].downcase + params[:user][:last_name].downcase
    usernames = User.pluck(:username)
    username = original_username
    i = 2
    while usernames.include? username do
      username = original_username + i.to_s
      i = i + 1
    end

    @user.username = username

    if params[:user][:suffix] == "Jr."
      params[:user][:suffix] = "junior"
    elsif params[:user][:suffix] == "Sr."
      params[:user][:suffix] = "senior"
    end

    @user.suffix = params[:user][:suffix]

    split_date = params[:user][:valid_till].split("/")
    if split_date != nil
      date = split_date[2] + "-" + split_date[0] + "-" + split_date[1]
      @user.valid_till = Date.parse(date)
    else
      @user.valid_till = Date.parse(split_date)
    end

    split_date = params[:user][:birthday].split("/")
    if split_date != nil
      date = split_date[2] + "-" + split_date[0] + "-" + split_date[1]
      @user.birthday = Date.parse(date)
    else
      @user.valid_till = Date.parse(split_date)
    end

    respond_to do |format|
      if !@user.save
        if params["user"]["password"] != params["user"]["confirm_password"]
          @user.errors.add(:base, "Password and Confirm Password does not match")
        end

        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      else
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      end
    end
  end

  def destroy
     if @user.admin?           #admin

      EmployeeList.where(user_id: @user.id).delete_all()
      Notification.where(subscribed_user_id: @user.id).delete_all() #delete all of his/her notifs in his/her notifs tab
      WorksIn.where(user_id: @user.id).delete_all()
      CalendarEvent.where(owner_id: @user.id).delete_all()
      @user.posts.delete_all() #delete all posts of user

    end
    if @user.teacher?           #teacher
      classes=ClassCourse.where(teacher_id:@user.id).update_all(teacher_id:nil)
      Notification.where(subscribed_user_id: @user.id).delete_all()
      WorksIn.where(user_id: @user.id).delete_all()
      TeacherList.where(user_id: @user.id).delete_all()
      CalendarEvent.where(owner_id: @user.id).delete_all()
      @user.posts.delete_all()
    end
    if @user.employee?           #employee

      Notification.where(subscribed_user_id: @user.id).delete_all()
      EmployeeList.where(user_id: @user.id).delete_all()
      WorksIn.where(user_id: @user.id).delete_all()
      CalendarEvent.where(owner_id: @user.id).delete_all()
      @user.posts.delete_all()
    end
    if @user.student?              #student

      ClassList.where(user_id: @user.id).delete_all()
      GroupList.where(user_id: @user.id).delete_all()
      Notification.where(subscribed_user_id: @user.id).delete_all()
      #Officer.where(user_id: @user.id).delete_all()
      Message.where(author_id:@user.id).delete_all()
      CalendarEvent.where(owner_id: @user.id).delete_all()
      @user.posts.delete_all()
    end
    @user.destroy

    respond_to do |format|
      format.html { redirect_to users_path, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    original_username = params[:user][:first_name][0].downcase + params[:user][:middle_name][0].downcase + params[:user][:last_name].downcase
    usernames = User.pluck(:username)
    username = original_username
    i = 2
    while usernames.include? username do
      username = original_username + i.to_s
      i = i + 1
    end

    @user.username = username

    if params[:user][:suffix] == "Jr."
      params[:user][:suffix] = "junior"
    elsif params[:user][:suffix] == "Sr."
      params[:user][:suffix] = "senior"
    end

    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/  1
  # DELETE /users/1.json

  def destroy_multiple
     delete_users=User.where(id: params[:user_ids])
     #
     delete_users.each do |user|
       if user.admin?           #admin

          EmployeeList.where(id: user.id).delete_all()
          Notification.where(subscribed_user_id: user.id).delete_all() #delete all of his/her notifs in his/her notifs tab
          WorksIn.where(user_id: user.id).delete_all()
          CalendarEvent.where(owner_id: user.id).delete_all()
          user.posts.delete_all() #delete all posts of user
          user.destroy!
       end
       if user.teacher?           #teacher

          a=ClassCourse.where(teacher_id:user.id)
          b=ClassCourse.where(teacher_id:user.id).update_all(teacher_id:nil)
          Notification.where(subscribed_user_id: user.id).delete_all()
          WorksIn.where(user_id: user.id).delete_all()
          TeacherList.where(user_id: user.id).delete_all()
          CalendarEvent.where(owner_id: user.id).delete_all()
          user.posts.delete_all() #delete all posts of user
          user.destroy!
       end
       if user.student?              #student

          ClassList.where(user_id: user.id).delete_all()
          GroupList.where(user_id: user.id).delete_all()
          Notification.where(subscribed_user_id: user.id).delete_all()
          #Officer.where(user_id: user.id).delete_all()
          CalendarEvent.where(owner_id: user.id).delete_all()
          #user.notifications.delete_all()
          Message.where(author_id:user.id).delete_all()
          user.officers.delete_all() #delete all officer position
          user.posts.delete_all()
          user.destroy!
       end
       if user.employee?           #employee
        Notification.where(subscribed_user_id: user.id).delete_all()
        EmployeeList.where(user_id: user.id).delete_all()
        WorksIn.where(user_id: user.id).delete_all()
        CalendarEvent.where(owner_id: user.id).delete_all()
        user.posts.delete_all()
        user.destroy!
      end
     end

     respond_to do |format|
       format.html { redirect_to users_path, notice: 'Users were successfully deleted.' }
       format.json { render :show, status: :ok, location: @user }
     end

  end

  def import
        a = User.import(params[:input_file])

        if a == false
          redirect_to users_path, notice: "Invalid File type"
        else
          redirect_to users_path, notice: "Users imported."
        end
  end
  def deletebybatch
          a = User.deletebybatch(params[:input_file])

        if a == false
          redirect_to users_path, notice: "Invalid File type"
        else
          redirect_to users_path, notice: "Users deleted."
        end
  end

  private
    # Use callbacks to share common setup or constraints between actions.

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:username, :email, :first_name, :middle_name, :last_name, :phone_number, :suffix, :valid_till, :role_id, :password, :password_confirmation, :street_address, :barangay, :postal_code, :province, :birthday, :guardian_name, :guardian_relationship, :guardian_contact_number)
    end
end
