class ThreadMessageTablesController < ApplicationController
  before_filter :authenticate_user!
  load_and_authorize_resource

  # GET /thread_message_tables
  # GET /thread_message_tables.json
  def index
    @thread_message_tables = ThreadMessageTable.all
  end

  # GET /thread_message_tables/1
  # GET /thread_message_tables/1.json
  def show
  end

  # GET /thread_message_tables/new
  def new
    @thread_message_table = ThreadMessageTable.new
  end

  # GET /thread_message_tables/1/edit
  def edit
  end

  # POST /thread_message_tables
  # POST /thread_message_tables.json
  def create
    @thread_message_table = ThreadMessageTable.new(thread_message_table_params)

    respond_to do |format|
      if @thread_message_table.save
        format.html { redirect_to group_view_path, notice: 'Thread message table was successfully created.' }
        format.json { render :show, status: :created, location: @thread_message_table }
      else
        format.html { render :new }
        format.json { render json: @thread_message_table.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /thread_message_tables/1
  # PATCH/PUT /thread_message_tables/1.json
  def update
    respond_to do |format|
      if @thread_message_table.update(thread_message_table_params)
        format.html { redirect_to group_view_path, notice: 'Thread message table was successfully updated.' }
        format.json { render :show, status: :ok, location: @thread_message_table }
      else
        format.html { render :edit }
        format.json { render json: @thread_message_table.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /thread_message_tables/1
  # DELETE /thread_message_tables/1.json
  def destroy
    @thread_message_table.destroy
    respond_to do |format|
      format.html { redirect_to group_view_path, notice: 'Thread message table was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_thread_message_table
      @thread_message_table = ThreadMessageTable.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def thread_message_table_params
      params.require(:thread_message_table).permit(:thread_id, :message_id)
    end
end
