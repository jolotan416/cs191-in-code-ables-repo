class WorksInsController < ApplicationController
  before_filter :authenticate_user!
  load_and_authorize_resource
#  before_action :set_works_in, only: [:show, :edit, :update, :destroy]

  # GET /works_ins
  # GET /works_ins.json
  def index
    @works_ins = WorksIn.all
  end

  # GET /works_ins/1
  # GET /works_ins/1.json
  def show
  end

  # GET /works_s/new
  def new
    @works_in = WorksIn.new
  end

  # GET /works_ins/1/edit
  def edit
  end

  # POST /works_ins
  # POST /works_ins.json
  def create
    @works_in = WorksIn.new
    @works_in.start_date = Time.now
    @works_in.user_id = params[:user_id][:user_id]
    @works_in.department_id = params[:department_id][:department_id]
    respond_to do |format|
      if @works_in.save
        format.html { redirect_to @works_in, notice: 'Works in was successfully created.' }
        format.json { render :show, status: :created, location: @works_in }
      else
        format.html { render :new }
        format.json { render json: @works_in.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /works_ins/1
  # PATCH/PUT /works_ins/1.json
  def update
    respond_to do |format|
      if @works_in.update(works_in_params)
        format.html { redirect_to @works_in, notice: 'Works in was successfully updated.' }
        format.json { render :show, status: :ok, location: @works_in }
      else
        format.html { render :edit }
        format.json { render json: @works_in.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /works_ins/1
  # DELETE /works_ins/1.json
  def destroy
    @works_in.destroy
    respond_to do |format|
      format.html { redirect_to works_ins_url, notice: 'Works in was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_works_in
      @works_in = WorksIn.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def works_in_params
      params.require(:works_in).permit(:start_date, :end_date, :deparment_id, :user_id)
    end
end
