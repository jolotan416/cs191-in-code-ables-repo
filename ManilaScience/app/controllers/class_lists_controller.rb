class ClassListsController < ApplicationController
  before_filter :authenticate_user!
  load_and_authorize_resource
#  before_filter :authenticate_user!
#  before_action :set_class_list, only: [:show, :edit, :update, :destroy]

  # GET /class_lists
  # GET /class_lists.json
  def index
    @class_lists = ClassList.all
    @users = User.all
  end

  # GET /class_lists/1
  # GET /class_lists/1.json
  def show
  #
  end

  # GET /class_lists/new
  def new
    respond_to do |format|
      format.html { redirect_to @class_list, notice: 'Class list was successfully updated.' }
      format.json { render :show, status: :ok, location: @class_list }
    end
  end

  def new_view
    session[:class_course_id] = params[:class_course_id]
    our_number = Role.where(name: "Students")
    @users = User.all.where(role_id: our_number)
    @users = @users.sort {|a, b| a.last_name <=> b.last_name}
  end

  def new_class_lists
    selected_users = params[:users]
    class_course_id = session[:class_course_id]
    class_lists = ClassList.where(:class_course_id => class_course_id).pluck(:user_id)
    selected_users.each do |selected_user|
      if !(class_lists.include? selected_user.to_i)
        @class_list = ClassList.create(:user_id => selected_user.to_s, :class_course_id => session[:class_course_id], :status => :enrolled)
      end
    end

    session[:class_course_id] = nil

    respond_to do |format|
      format.html { redirect_to ("/class_courses/" + class_course_id.to_s), notice: 'Class list was successfully updated.' }
      format.json { render :show, status: :ok, location: @class_list }
    end
  end

  # GET /class_lists/1/edit
  def edit
  end

  # POST /class_lists
  # POST /class_lists.json
  def create
    @class_list = ClassList.new(class_list_params)
    @class_list.user_id = session[:user_id]
    @class_list.class_course_id = session[:class_course_id]

    respond_to do |format|
      if @class_list.save
        format.html { redirect_to @class_list, notice: 'Class list was successfully created.' }
        format.json { render :show, status: :created, location: @class_list }
      else
        format.html { render :new }
        format.json { render json: @class_list.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /class_lists/1
  # PATCH/PUT /class_lists/1.json
  def update
    @class_list.user_id = params[:user_id][:user_id]
    @class_list.class_courses_id = params[:class_courses_id][:class_courses_id]
    respond_to do |format|
      if @class_list.update(class_list_params)
        format.html { redirect_to @class_list, notice: 'Class list was successfully updated.' }
        format.json { render :show, status: :ok, location: @class_list }
      else
        format.html { render :edit }
        format.json { render json: @class_list.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /class_lists/1
  # DELETE /class_lists/1.json
  def destroy
    class_course_id = @class_list.class_course_id
    @class_list.destroy
    respond_to do |format|
      format.html { redirect_to ("/class_courses/" + class_course_id.to_s), notice: 'Class list was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_class_list
      @class_list = ClassList.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def class_list_params
      params.require(:class_list).permit(:user_id, :class_courses_id, :status)
    end
end
