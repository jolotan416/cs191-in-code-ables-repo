class Users::SessionsController < Devise::SessionsController
  def create
    # 
    self.resource = warden.authenticate!(auth_options)
    if resource.valid_till <= Time.zone.now
      sign_out
      flash[:negative] = "Sorry. The User Account " + self.resource.username.to_s + " is already expired."
      redirect_to main_app.root_url
    else	
 	   super
 	end
  end
end