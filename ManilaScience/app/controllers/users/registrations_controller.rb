class Users::RegistrationsController < Devise::RegistrationsController
	before_filter :configure_permitted_parameters
	
	def create
		# Customize anything about sign_up_params here

		# Calls the original code for function create
		super
		
	end

	protected

	def configure_permitted_parameters
		devise_parameter_sanitizer.for(:sign_up).push(:first_name, :email)
	end
end