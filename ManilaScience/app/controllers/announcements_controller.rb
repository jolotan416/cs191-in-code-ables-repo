class AnnouncementsController < ApplicationController
  before_filter :authenticate_user!
  load_and_authorize_resource

  # GET /announcements
  # GET /announcements.json
  def index

    # This condition toggles visibility of the announcements.
    # If the user is an admin, I want it to be able to view
    # all announcements so as to easily manage them. (Might set visibility of hidden announcements to true)

    if(current_user.admin?)
      @announcements = Announcement.all
    else
      @announcements = Announcement.where(:visibility => true)
    end
    @announcements = @announcements.sort_by {|announcement| announcement.edit_time}.reverse
  end

  # GET /announcements/1
  # GET /announcements/1.json
  def show
  end

  # GET /announcements/new
  def new
    @announcement = Announcement.new
    @users = User.all
  end

  # GET /announcements/1/edit
  def edit
    @users = User.all
  end

  # POST /announcements
  # POST /announcements.json
  def create
    @announcement = Announcement.new(announcement_params)
    @announcement.author_id = current_user.id
    @announcement.edit_time = DateTime.now
    @announcement.visibility = true

    respond_to do |format|
      if @announcement.save
        create_notification(@announcement,4)
        format.html { redirect_to @announcement, notice: 'Announcement was successfully created.' }
        format.json { render :show, status: :created, location: @announcement }
      else
        format.html { render :new }
        format.json { render json: @announcement.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /announcements/1
  # PATCH/PUT /announcements/1.json
  def update
    @announcement.author_id = current_user.id
    @announcement.edit_time = DateTime.now
    @announcement.visibility = true

    respond_to do |format|
      if @announcement.update(announcement_params)
        #create_notification(@announcement,5)
        format.html { redirect_to @announcement, notice: 'Announcement was successfully updated.' }
        format.json { render :show, status: :ok, location: @announcement }
      else
        format.html { render :edit }
        format.json { render json: @announcement.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /announcements/1
  # DELETE /announcements/1.json
  def destroy
    @announcement_temp=@announcement.dup  #duplicate instance variable before deleting
    #create_notification(@announcement_temp,6)
    Notification.where(announcement_id:@announcement.id).delete_all() #delete all of the notifications of that announcement
    @announcement.destroy
    respond_to do |format|
      format.html { redirect_to announcements_url, notice: 'Announcement was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_announcement
      @announcement = Announcement.find(params[:id ])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def announcement_params
      params.require(:announcement).permit(:title, :body, :edit_time, :visibility, :author_id)
    end

    def create_notification(announcement,kind)
      #return if post.user.id==current_user.id
      @notify_users=User.all

      if @notify_users!=nil
        @notify_users.each do |u|
          #()
          if u.id!=announcement.author_id
            a=Notification.create(user_id:u.id,subscribed_user_id:announcement.author_id, identifier: kind,read: false,announcement_id: announcement.id)
          end# end if not him/herself
        end#enf loop
      end #end notify users
    end #end create _notification
  end#end destroy
