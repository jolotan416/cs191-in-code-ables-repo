class NotificationsController < ApplicationController
  def link_through
		@notification = Notification.find(params[:id])
	  @notification.update read: true
	  redirect_to post_path @notification.post
  end
  def index
  	@notifications=current_user.notifications.order('created_at DESC')
  end
  def read_post
    @notification = Notification.find(params[:id])
    @notification.read = true
    respond_to do |format|
      if @notification.identifier.to_i >= 4
        format.html { redirect_to "/announcements/" + @notification.post_id.to_s, notice: 'Announcement was successfully created.' }
      else
        format.html { redirect_to "/posts/" + @notification.post_id.to_s, notice: 'Announcement was successfully created.' }
      end
      format.json { render :show, status: :created, location: @announcement }
    end
  end
end
