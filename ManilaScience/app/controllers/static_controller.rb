class StaticController < ApplicationController
  before_filter :authenticate_user!
  def index

    @calendar_events = CalendarEvent.all


    # This condition toggles visibility of the announcements.
    # If the user is an admin, I want it to be able to view
    # all announcements so as to easily manage them. (Might set visibility of hidden announcements to true)

    if(current_user.admin?)
	    @announcements = Announcement.all
  	else
  		@announcements = Announcement.where(:visibility => true)
  	end
    @announcements = @announcements.sort_by {|announcement| announcement.edit_time}.reverse
    @announcements = @announcements[0..2]
  end
end
