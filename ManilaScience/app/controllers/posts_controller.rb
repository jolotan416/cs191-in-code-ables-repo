class PostsController < ApplicationController
  before_filter :authenticate_user!
  load_and_authorize_resource
#  before_action :set_post, only: [:show, :edit, :update, :destroy]
#  before_action :owned_post, only: [:edit, :update, :destroy]
  # GET /posts
  # GET /posts.json
  def owned_post
    unless current_user == @post.user
      flash[:alert] = "That post doesn't belong to you!"
      redirect_to root_path
    end
  end
  def index
    @posts = Post.all
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
    @id = params[:course_id]
  end

  # GET /posts/new
  def new
    session[:id] = params[:id]
    @id = params[:id]
    @announcement = params[:announcement]
    @post = current_user.posts.new
  end

  # GET /posts/1/edit
  def edit
    session[:id] = params[:course_id]
    @id = params[:course_id]
  end

  # POST /posts
  # POST /posts.json
  def create
    @post = current_user.posts.new(post_params)
    @post.user_id = current_user.id

    @post.class_course_id = session[:id]
    @id = session[:id]
    session[:id] = nil
    respond_to do |format|
      if @post.save
        #create_notification(@post,"posted")
        create_notification(@post,1)
        if Role.find(current_user.role_id.to_s).name != "Admin"
          if ClassCourse.find(@id.to_s).is_club == true
            format.html { redirect_to club_view_view_club_path(:class_course_id => @id.to_s), notice: 'Post was successfully created.' }
          else
            format.html { redirect_to class_view_view_class_path(:class_course_id => @id.to_s), notice: 'Post was successfully created.' }
          end
        else
          format.html { redirect_to "/class_courses/" + @id.to_s, notice: 'Post was successfully created.' }
        end
        format.json { render :show, status: :created, location: @post }
      else
        format.html { render :new }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /posts/1
  # PATCH/PUT /posts/1.json
  def update
    @post.user_id = current_user.id
    @post.class_course_id = session[:id]
    @id = session[:id]
    session[:id] = nil

    respond_to do |format|
      if @post.update(post_params)
        create_notification(@post,2)
        format.html { redirect_to "/class_courses/" + @id.to_s, notice: 'Post was successfully updated.' }
        format.json { render :show, status: :ok, location: @post }
      else
        format.html { render :edit }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    #create_notification(@post,3)
    @notify_users=ClassList.where(class_course_id:@post.class_course_id)
    if @notify_users!=nil
        @notify_users.each do |u|
        if u.user_id!=@post.user_id
            a=Notification.create(user_id:u.user_id,subscribed_user_id:@post.user_id,class_course_id:@post.class_course_id, identifier:3,read: false)

          end
        end
    end
    @id = params[:course_id]
    @post.destroy
    respond_to do |format|
      format.html { redirect_to "/class_courses/" + @id.to_s, notice: 'Post was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def post_params
      params.require(:post).permit(:caption,:document, :user_id, :class_course_id)
    end
    def create_notification(post,kind)
      #return if post.user.id==current_user.id
      @notify_users=ClassList.where(class_course_id:post.class_course_id)
      if @notify_users!=nil
        @notify_users.each do |u|
        if u.user_id!=post.user_id
            a=Notification.create(user_id:u.user_id,subscribed_user_id:post.user_id,post_id:post.id,class_course_id:post.class_course_id, identifier: kind,read: false)

          end
        end
      end
    end

end
