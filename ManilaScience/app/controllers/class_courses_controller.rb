class ClassCoursesController < ApplicationController
  before_filter :authenticate_user!
  load_and_authorize_resource
#  before_filter :authenticate_user!
#  before_action :set_class_course, only: [:show, :edit, :update, :destroy]

  # GET /class_courses
  # GET /class_courses.json
  def index
    @class_courses = ClassCourse.all
    @subjects = Subject.all
  end

  # GET /class_courses/1
  # GET /class_courses/1.json
  def show
    @id = params[:id]
    @subject = Subject.all
    @users = User.all
    @classes_lists = ClassList.where(:class_course_id => @id)
    @posts = Post.where(:class_course_id => @id)
  end

  # GET /class_courses/new
  def new
    @class_course = ClassCourse.new
    session[:is_club] = false
  end

  def new_club
    @class_course = ClassCourse.new
    session[:is_club] = true
  end

  # GET /class_courses/1/edit
  def edit
  end

  # POST /class_courses
  # POST /class_courses.json
  def create
    @class_course = ClassCourse.new(class_course_params)
    @class_course.subject_id = params[:subject_id][:subject_id]
    @class_course.teacher_id = params[:teacher_id][:id]
    @class_course.is_club = session[:is_club]

    days = ""
    schedule = params[:class_course][:schedule]
    days_abbrev = ["M", "T", "W", "TH", "F", "SA", "SU"]
    days_abbrev.each do |d|
      if schedule.include? d
        days = days + d
      end
    end

    @class_course.schedule = days + " " + params[:start_time] + "-" + params[:end_time]
    @class_course.name = @class_course.year.to_s + params[:section]

    respond_to do |format|
      if @class_course.save
        session[:is_club] = nil
        format.html { redirect_to @class_course, notice: 'Class/Club was successfully created.' }
        format.json { render :show, status: :created, location: @class_course }
      else
        format.html { render :new }
        format.json { render json: @class_course.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /class_courses/1
  # PATCH/PUT /class_courses/1.json
  def update
    @class_course.subject_id = params[:subject_id][:subject_id]
    @class_course.teacher_id = params[:teacher_id][:id]

    days = ""
    schedule = params[:class_course][:schedule]
    days_abbrev = ["M", "T", "W", "TH", "F", "SA", "SU"]
    days_abbrev.each do |d|
      if schedule.include? d
        days = days + d
      end
    end

    @class_course.schedule = days + " " + params[:start_time] + "-" + params[:end_time]
    @class_course.name = @class_course.year.to_s + "-" + params[:section]

    respond_to do |format|
      if @class_course.update(class_course_params)
        format.html { redirect_to @class_course, notice: 'Class course was successfully updated.' }
        format.json { render :show, status: :ok, location: @class_course }
      else
        format.html { render :edit }
        format.json { render json: @class_course.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /class_courses/1
  # DELETE /class_courses/1.json
  def destroy
    Group.where(class_id:@class_course.id).delete_all()     #delete all groups inside that class
    ClassList.where(class_course_id:@class_course.id).delete_all() #delete all the related class_list to it
    @class_course.destroy
    respond_to do |format|
      format.html { redirect_to class_courses_url, notice: 'Class course was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_class_course
      @class_course = ClassCourse.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def class_course_params
      params.require(:class_course).permit(:name, :room, :year, :schedule, :size, :subject_id, :new, :teacher_id, :is_club)
    end
end
