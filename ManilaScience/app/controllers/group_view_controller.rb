class GroupViewController < ApplicationController
	before_filter :authenticate_user!

	def index
	    @message = Message.new
	    if current_user.admin?
	    	@class_courses = ClassCourse.all.where(:is_club => false)
	    	@club_courses = ClassCourse.all.where(:is_club => true)
		elsif current_user.teacher?
			@class_courses = ClassCourse.where(:teacher_id => current_user.id).where(:is_club => false)
			@club_courses = ClassCourse.where(:teacher_id => current_user.id).where(:is_club => true)

		elsif current_user.student?
			# Let's find the class and courses where the students is enrolled. We can find this in the class_lists
			@class_lists = ClassList.where(:user_id => current_user.id)
			# Let's extract the class courses from the class_lists
			@class_courses = Array.new
			@club_courses = Array.new

# ClassCourse.find_by_id(class_list.class_course_id).is_club

			@class_lists.each do |class_list|
				if ClassCourse.find_by_id(class_list.class_course_id).is_club
					@club_courses.push(ClassCourse.find_by_id(class_list.class_course_id))
				else
					@class_courses.push(ClassCourse.find_by_id(class_list.class_course_id))
				end
			end
		end
	end

	def edit_student_lists
		# Students are not allowed beyond this point
		if current_user.student?
			redirect_to root_path, notice: 'Access denied.'
		end

		@group = Group.find(params[:group])

		# Let's check if the teacher is indeed the teacher of the class course.

		@class_course = ClassCourse.find_by_id(@group.class_id)

		if current_user.teacher? and current_user.id != @class_course.teacher_id
			# This means that you are not the teacher of this class. You must not be able to access this. Sorry.
			redirect_to root_path, notice: 'Access denied.'
		end

		# Now, we are ready to render all the students belonging in this class
		@class_list = ClassList.where(:class_course_id => @class_course.id)
		@group_list = GroupList.where(:group_id => @group.id)

		@user_in_group = Array.new
		@user_not_in_group = Array.new

		# These are all those that are already in the group
		@group_list.each do |group_list|
			@user_in_group.push(User.find_by_id(group_list.user_id))
		end

		# Scan for those na wala sa group
		@class_list.each do |class_list|
			if !@user_in_group.include? User.find(class_list.user_id)
				@user_not_in_group.push(User.find_by_id(class_list.user_id))
			end
		end
	end

	def add_user_from_group
		selected_users = params["user_in_group"]
	    selected_users.each do |selected_user|
	    	@group_list = GroupList.create(:user_id => selected_user.to_s, :group_id => params["group_id"])
	    end
	    respond_to do |format|
	      format.html { redirect_to ("/group_view/edit_student_lists?group=" + params["group_id"].to_s), notice: 'Users were successfully added.' }
	      format.json { render :show, status: :ok, location: @class_list }
	    end
	end

	def remove_user_from_group
		selected_users = params["user_in_group"]
	    selected_users.each do |selected_user|
	    	@group_list = GroupList.where(:user_id => selected_user.to_s, :group_id => params["group_id"])
	    	@group_list.each do |group_list|
		    	GroupList.destroy(group_list.id)
	    	end
	    end

	    respond_to do |format|
	      format.html { redirect_to ("/group_view/edit_student_lists?group=" + params["group_id"].to_s), notice: 'Users were successfully removed.' }
	      format.json { render :show, status: :ok, location: @class_list }
	    end
	end
end
