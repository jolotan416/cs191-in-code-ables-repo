class ScheduleViewController < ApplicationController
	before_filter :authenticate_user!
	def index
		@calendar_events = CalendarEvent.all
	end
end
