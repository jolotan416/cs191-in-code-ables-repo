class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
	before_filter :configure_permitted_parameters, if: :devise_controller?

	rescue_from CanCan::AccessDenied do |exception|
		flash[:error] = "Access Denied!"
		redirect_to static_index_path
	end

	def after_sign_in_path_for(resource_or_scope)
	  static_index_path
	end

	def after_sign_out_path_for(resource_or_scope)
	  "http://www.manilascience.edu.ph/"
	end

	protected
	def configure_permitted_parameters
	  devise_parameter_sanitizer.for(:sign_up) << :name
	  devise_parameter_sanitizer.for(:account_update) << :name
	end
end
