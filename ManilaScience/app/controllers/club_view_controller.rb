class ClubViewController < ApplicationController
	def index
		@calendar_events = CalendarEvent.all

		# This is the ClassList Model
		if Role.find(current_user.role_id).name == "Students"
			@clubs_temp = ClassList.where(:user_id => current_user.id)
		else
			@clubs_temp = ClassCourse.where(:teacher_id => current_user.id)
		end
		# 
		for i in (0...@clubs_temp.length)
			@clubs_temp[i] = @clubs_temp[i].attributes
			if Role.find(current_user.role_id).name != "Students"
				@clubs_temp[i]["class_course_id"] = @clubs_temp[i]["id"]
			end
		end
		# We need to find what is the user's section and class adviser

		for i in (0...@clubs_temp.length)
			# We are now joining the ClassList Model to the Course Model
			@course = ClassCourse.find(@clubs_temp[i]["class_course_id"])
			# 
			@clubs_temp[i]["room"] = @course.room
			@clubs_temp[i]["year"] = @course.year
			@clubs_temp[i]["schedule"] = @course.schedule
			@clubs_temp[i]["size"] = @course.size
			@clubs_temp[i]["subject_id"] = @course.subject_id
			# @clubs_temp[i]["created_at"] = @course.created_at
			# @clubs_temp[i]["updated_at"] = @course.updated_at
			@clubs_temp[i]["name"] = @course.name
			@clubs_temp[i]["teacher_id"] = @course.teacher_id
			@clubs_temp[i]["is_club"] = @course.is_club

			# We are now joining the ClassList Model to the Course Model to the Subject
			@subject = Subject.find(@clubs_temp[i]["subject_id"])
			@clubs_temp[i]["subject_name"] = @subject.subject_name
			@clubs_temp[i]["offered"] = @subject.offered
			@clubs_temp[i]["department_id"] = @subject.department_id

			# @clubs_temp[i]["subj_created_at"] = @subject.created_at
			# @clubs_temp[i]["subj_updated_at"] = @subject.created_at

		end

		@clubs = Array.new
		for i in (0...@clubs_temp.length)
			if @clubs_temp[i]["is_club"] == true
				@clubs.append(@clubs_temp[i])
			end
		end
	end

	def view_club
		
		@id = params[:class_course_id]
		@class_course = ClassCourse.find_by_id(@id)
		@subject = Subject.all
		@users = User.all
		@classes_lists = ClassList.where(:class_course_id => @id)
		@posts = Post.where(:class_course_id => @id)

		#
	end
end
