class TeacherListsController < ApplicationController
  before_filter :authenticate_user!
  load_and_authorize_resource
#  before_action :set_teacher_list, only: [:show, :edit, :update, :destroy]

  # GET /teacher_lists
  # GET /teacher_lists.json
  def index
    @teacher_lists = TeacherList.all
    @users = User.all
  end

  # GET /teacher_lists/1
  # GET /teacher_lists/1.json
  def show
  end

  # GET /teacher_lists/new
  def new
    @teacher_list = TeacherList.new
  end

  # GET /teacher_lists/1/edit
  def edit
  end

  # POST /teacher_lists
  # POST /teacher_lists.json
  def create
    @teacher_list = TeacherList.new(teacher_list_params)
    @teacher_list.user_id = params[:user][:user_id];

    respond_to do |format|
      if @teacher_list.save
        format.html { redirect_to @teacher_list, notice: 'Teacher list was successfully created.' }
        format.json { render :show, status: :created, location: @teacher_list }
      else
        format.html { render :new }
        format.json { render json: @teacher_list.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /teacher_lists/1
  # PATCH/PUT /teacher_lists/1.json
  def update
    respond_to do |format|
      @teacher_list.user_id = params[:user_id][:user_id];
      if @teacher_list.update(teacher_list_params)
        format.html { redirect_to @teacher_list, notice: 'Teacher list was successfully updated.' }
        format.json { render :show, status: :ok, location: @teacher_list }
      else
        format.html { render :edit }
        format.json { render json: @teacher_list.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /teacher_lists/1
  # DELETE /teacher_lists/1.json
  def destroy
    ClassCourse.where(teacher_id:userid).update_all(teacher_id:nil)
    Notification.where(subscribed_user_id: user.id).delete_all()
    WorksIn.where(user_id: user.id).delete_all()
    CalendarEvent.where(owner_id: user.id).delete_all()
    user.posts.delete_all() #delete all posts of user
    user.delete
    @teacher_list.destroy
    respond_to do |format|
      format.html { redirect_to teacher_lists_url, notice: 'Teacher list was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_teacher_list
      @teacher_list = TeacherList.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def teacher_list_params
      params.require(:teacher_list).permit(:title, :user_id)
    end
end
