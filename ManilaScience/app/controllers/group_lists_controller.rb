class GroupListsController < ApplicationController
  before_filter :authenticate_user!
  load_and_authorize_resource
#  before_action :set_group_list, only: [:show, :edit, :update, :destroy]

  # GET /group_lists
  # GET /group_lists.json
  def index
    @group_lists = GroupList.all
  end

  # GET /group_lists/1
  # GET /group_lists/1.json
  def show
  end

  # GET /group_lists/new
  def new
    @group_list = GroupList.new
  end

  # GET /group_lists/1/edit
  def edit
  end

  # POST /group_lists
  # POST /group_lists.json
  def create
    @group_list = GroupList.new(group_list_params)
    
    @group_list.user_id = params["user_id"]["user_id"]
    @group_list.group_id = params["group_id"]["group_id"]
    
    respond_to do |format|
      if @group_list.save
        format.html { redirect_to @group_list, notice: 'Group list was successfully created.' }
        format.json { render :show, status: :created, location: @group_list }
      else
        format.html { render :new }
        format.json { render json: @group_list.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /group_lists/1
  # PATCH/PUT /group_lists/1.json
  def update
    respond_to do |format|
      if @group_list.update(group_list_params)
        format.html { redirect_to @group_list, notice: 'Group list was successfully updated.' }
        format.json { render :show, status: :ok, location: @group_list }
      else
        format.html { render :edit }
        format.json { render json: @group_list.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /group_lists/1
  # DELETE /group_lists/1.json
  def destroy
    @group_list.destroy
    respond_to do |format|
      format.html { redirect_to group_lists_url, notice: 'Group list was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_group_list
      @group_list = GroupList.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def group_list_params
      # params.require(:group_list).permit(:user_id, :group_id)
    end
end
