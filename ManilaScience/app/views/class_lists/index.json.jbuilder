json.array!(@class_lists) do |class_list|
  json.extract! class_list, :id, :user_id, :class_courses_id, :status
  json.url class_list_url(class_list, format: :json)
end
