json.array!(@class_courses) do |class_course|
  json.extract! class_course, :id, :room, :year, :schedule, :size, :subject_id
  json.url class_course_url(class_course, format: :json)
end
