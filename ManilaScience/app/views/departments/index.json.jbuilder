json.array!(@departments) do |department|
  json.extract! department, :id, :name, :current_size, :capacity
  json.url department_url(department, format: :json)
end
