json.array!(@works_ins) do |works_in|
  json.extract! works_in, :id, :start_date, :end_date, :deparment_id, :user_id
  json.url works_in_url(works_in, format: :json)
end
