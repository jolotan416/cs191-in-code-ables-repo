json.array!(@officers) do |officer|
  json.extract! officer, :id, :user_id, :position, :class_course_id, :from, :to, :assigned_by
  json.url officer_url(officer, format: :json)
end
