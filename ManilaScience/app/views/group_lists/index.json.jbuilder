json.array!(@group_lists) do |group_list|
  json.extract! group_list, :id, :user_id, :group_id
  json.url group_list_url(group_list, format: :json)
end
