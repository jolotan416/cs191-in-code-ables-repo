json.array!(@subjects) do |subject|
  json.extract! subject, :id, :subject_name, :offered, :department_id
  json.url subject_url(subject, format: :json)
end
