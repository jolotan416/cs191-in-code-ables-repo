json.extract! @user, :id, :username, :email, :first_name, :middle_name, :last_name, :phone_number, :suffix, :valid_till, :role_id, :created_at, :updated_at
