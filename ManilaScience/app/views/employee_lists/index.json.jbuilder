json.array!(@employee_lists) do |employee_list|
  json.extract! employee_list, :id, :title, :user_id
  json.url employee_list_url(employee_list, format: :json)
end
