json.array!(@messages) do |message|
  json.extract! message, :id, :author_id, :content, :flag
  json.url message_url(message, format: :json)
end
