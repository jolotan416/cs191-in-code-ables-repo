json.array!(@teacher_lists) do |teacher_list|
  json.extract! teacher_list, :id, :title, :user_id
  json.url teacher_list_url(teacher_list, format: :json)
end
