json.array!(@thread_message_tables) do |thread_message_table|
  json.extract! thread_message_table, :id, :thread_id, :message_id
  json.url thread_message_table_url(thread_message_table, format: :json)
end
