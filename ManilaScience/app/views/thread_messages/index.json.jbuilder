json.array!(@thread_messages) do |thread_message|
  json.extract! thread_message, :id, :thread_name, :string, :group_id, :expiry_date
  json.url thread_message_url(thread_message, format: :json)
end
