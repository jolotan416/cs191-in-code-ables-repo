json.array!(@announcements) do |announcement|
  json.extract! announcement, :id, :title, :body, :edit_time, :visibility, :author_id
  json.url announcement_url(announcement, format: :json)
end
