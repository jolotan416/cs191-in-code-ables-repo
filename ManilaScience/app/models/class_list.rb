class ClassList < ActiveRecord::Base
  belongs_to :user
  belongs_to :class_course
  validates :user_id, presence: true
  validates :class_course_id, presence: true
  validates :status , inclusion: { in: ['enrolled', 'not enrolled'] }
  
end
