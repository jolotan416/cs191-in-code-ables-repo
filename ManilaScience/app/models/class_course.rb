class ClassCourse < ActiveRecord::Base
  belongs_to :subject
  has_many :posts
  has_many :officers
  has_many :class_list , dependent: :destroy
  has_many :notifications, dependent: :destroy
  #validates :name,presence:true,length: { minimum: 3,maximum: 10, too_short: "must have at least %{count} characters for class name" , too_long: "%{count} characters is the maximum allowed for class name" }
  validates :subject_id, presence: true
  #validates :teacher_id, presence: true
  validates :size, numericality: { less_than_or_equal_to: 50,greater_than_or_equal_to: 1, only_integer: true }, presence: true
  validates :room, length: { minimum: 3,maximum: 10, too_short: "must have at least %{count} characters for room" , too_long: "%{count} characters is the maximum allowed for room" }, presence: true
  validates :schedule, length: { minimum:11,maximum: 30,too_short: "must have at least %{count} characters for schedule" , too_long: "%{count} characters is the maximum characters allowed for schedule" }, presence: true
  #validates :is_club,presence: true
end
