class Post < ActiveRecord::Base
	 has_many :notifications, dependent: :destroy
	 belongs_to :user
	 validates :user_id, presence: true  
	 validates :caption,length: { maximum: 140 , too_long: "%{count} characters is the maximum allowed for caption/message" } ,presence: true
  	 has_attached_file :document, :styles => { :large => "600x600",:medium => "300x300>", :thumb => "100x100>" }
	 validates_attachment :document, :content_type => {:content_type => %w(image/jpeg image/jpg image/png application/pdf application/msword application/vnd.openxmlformats-officedocument.wordprocessingml.document)}
end
