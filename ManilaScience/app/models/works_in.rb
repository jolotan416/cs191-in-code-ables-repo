 class WorksIn < ActiveRecord::Base
  belongs_to :deparment
  belongs_to :user
  validates :user_id, presence: true
  validates :start_date, presence: true
  validates :department_id, presence: true  
end
