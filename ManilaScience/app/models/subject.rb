class Subject < ActiveRecord::Base
  belongs_to :department
  validates :subject_name,length: { maximum: 50 , too_long: "%{count} characters is the maximum allowed for subject name" },uniqueness: true,presence: true 
  validates :department_id, presence: true
  validates :offered,presence: true
  
end
