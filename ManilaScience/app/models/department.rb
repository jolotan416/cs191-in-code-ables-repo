class Department < ActiveRecord::Base
  validates :name, length: { maximum: 30, too_long: "%{count} characters is the maximum allowed for department name" }, presence: true, uniqueness: true
  validates :capacity, numericality: { greater_than_or_equal_to: 1,less_than_or_equal_to: 1000, only_integer: true }, presence: true
end
