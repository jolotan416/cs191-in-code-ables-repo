class Role < ActiveRecord::Base
  validates :name, uniqueness: true
  has_many :users
  validates :description,length: { maximum: 50 , too_long: "%{count} characters is the maximum allowed for caption/message" } ,presence: true
end
