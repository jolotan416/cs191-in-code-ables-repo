class Group < ActiveRecord::Base
	validates :group_name, presence: true, uniqueness: true,length: { maximum: 20 , too_long: "%{count} characters is the maximum allowed for group name" }
	validates :group_description,presence: true,length: { maximum: 60 , too_long: "%{count} characters is the maximum allowed for group description" }
	validates :class_id, presence: true
	
end
