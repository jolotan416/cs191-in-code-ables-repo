class Notification < ActiveRecord::Base
  belongs_to :user
  belongs_to :subscribes,class_name: 'User' ,foreign_key: 'subscribed_user'
  belongs_to :post
  belongs_to :class_course
  belongs_to :announcement

  validates :user_id,presence:true
  validates :subscribed_user_id,presence:true
  #validates :post_id,presence:true
  validates :identifier,presence: true , :inclusion => {:in => [1,2,3,4]}
 
end
