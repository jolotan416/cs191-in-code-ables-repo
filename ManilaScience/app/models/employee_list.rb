class EmployeeList < ActiveRecord::Base
  belongs_to :user
  validates :user_id, presence: true, uniqueness: true
  validates :title, length: { maximum: 20 , too_long: "%{count} characters is the maximum allowed for title" }, presence: true
end
