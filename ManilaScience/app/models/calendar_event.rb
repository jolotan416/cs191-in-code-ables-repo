class CalendarEvent < ActiveRecord::Base
  def personal_event?
    self.event_type == "Personal"
  end
  def class_event?
    self.event_type == "Class"
  end
  def club_event?
    self.event_type == "Club"
  end
  def school_event?
    self.event_type == "School"
  end
  def system_event?
    self.event_type == "System"
  end
  validates :title, presence: true , length: { maximum: 50, too_long: "%{count} characters is the maximum allowed for title" }
  validates :description, presence: true, length: { maximum: 140, too_long: "%{count} characters is the maximum allowed for description" }
  validates :end_time, presence: true
  validates :start_time, presence: true
  validates :event_type, presence: true

  def set_personal
    self.event_type = "Personal"
  end

  def set_class
    self.event_type = "Class"
  end
  
  def set_club
    self.event_type = "Club"
  end
  
  def set_school
    self.event_type = "School"
  end
  
  def set_system
    self.event_type = "System"
  end
  
end
