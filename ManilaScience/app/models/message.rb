class Message < ActiveRecord::Base
	#belongs_to :user
	has_many :thread_message_tables ,dependent: :destroy
	validates :author_id, presence: true
	validates :content,presence: true,length: { maximum: 140 , too_long: "%{count} characters is the maximum allowed for message content" }
		
end
