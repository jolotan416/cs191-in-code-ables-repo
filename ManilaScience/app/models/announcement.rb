class Announcement < ActiveRecord::Base
	has_many :notifications, dependent: :destroy
	validates :title, presence: true, length: { maximum: 50, too_long: "%{count} characters is the maximum allowed for title" },uniqueness: true
	validates :body, presence: true, length: { maximum: 150, too_long: "%{count} characters is the maximum allowed for body" }
	validates :visibility, presence: true 
	validates :author_id,presence: true
	
end
