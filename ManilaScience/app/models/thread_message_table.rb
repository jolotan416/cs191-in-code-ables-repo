class ThreadMessageTable < ActiveRecord::Base
	belongs_to :thread_message
	validates :message_id,presence:true
	validates :thread_id,presence:true
	
end
