class TeacherList < ActiveRecord::Base
  belongs_to :user
  validates :user_id, presence: true, uniqueness: true
  validates :title, length: { maximum: 30 , too_long: "%{count} characters is the maximum allowed for teacher's title" } ,presence: true
end
