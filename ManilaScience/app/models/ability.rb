class Ability
  include CanCan::Ability

  # user ||= User.new # guest user (not logged in)
  
  def initialize(user)
    if user.admin?
        can :manage, :all
    elsif user.teacher?
        can :read, Announcement
        can :manage, Group
        can :manage, ThreadMessage
        can :manage, Message
        can :create, CalendarEvent
        can :manage, GroupList
        can :manage, Post
        can :manage, ClassCourse
        can :manage, Subject
        
        # Lagay special kay teacher
    elsif user.student?
        can :read, Announcement
        can :create, CalendarEvent
        can :create, Message
        can :read, Post
        # Lagay tayo special if-else block dito for club officers
        # can :read, CalendarEvent, user.id =
    else
#      can :read, :all
    end


    # Note these ar eall crud...

    # if user.admin?
    #   can :manage, :all
    # elsif user.seller?
    #   can :read, Item
    #   can :create, Item
    #   can :update, Item do |item|
    #    item.try(:user) == user
    #   end
    #  can :destroy, Item do |item|
    #    item.try(:user) == user
    #  end
    # elsif user.regular?
    #   can :read, Item
    # end

    # Define abilities for the passed in user here. For example:
    #
    #   
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end
end
