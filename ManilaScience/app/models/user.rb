class User < ActiveRecord::Base
  require 'csv'
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  devise :database_authenticatable, :registerable,:recoverable, :rememberable, :trackable, :validatable, :timeoutable
  belongs_to :role
  before_save :assign_role
  validates :username,  presence: true, length: { minimum: 4, maximum:20, too_short: "must have at least %{count} characters for username",
    too_long: "must have at most %{count} characters for userame"},uniqueness: true
  validates :first_name,  presence: true, length: { minimum: 2, maximum:20, too_short: "must have at least %{count} characters for last name",
    too_long: "must have at most %{count} characters for last name"}
  validates :last_name,  presence: true, length: { minimum: 2, maximum:20, too_short: "must have at least %{count} characters for last name",
    too_long: "must have at most %{count} characters for last name"}
  validates :middle_name,  presence: true, length: { minimum: 2, maximum:20, too_short: "must have at least %{count} characters for middle name",
    too_long: "must have at most %{count} for middle name"}
  validates :phone_number, presence: true, length: {is: 11, wrong_length: " must have exactly %{count} characters for phone number"}  # exactly 11 digits i.e 09111..
  validates :email, presence: false, length: { minimum: 5, maximum: 320, too_short: "must have at least %{count} characters for email",
    too_long: "must have at most %{count} characters for email"}, #local@domain local part is up to 64 char long while domain is up to 255 char
                    format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }   #unique email addresses only
  validates :suffix, inclusion: {in: %w(junior senior I II III IV V VI),message: "%{value} is not a valid suffix in (junior senior I II III IV V VI) options"}, allow_blank:true,allow_nil:true #, length:{ minimum: 2} , inclusion: {in: %w(junior senior I II III IV V VI),message: "%{value} is not a valid suffix in (junior senior I II III IV V VI) options"}
  validates :password, presence: true, length: { minimum: 6, maximum:20, too_short: "must have at least %{count} characters for password",
    too_long: "must have at most %{count} characters for password"} , allow_nil:false
  validates :valid_till, presence:true
  validates :birthday, presence:true
  validates :postal_code, presence:true, numericality: { only_integer: true }, length:{ minimum: 2, maximum:5, too_short: "must have at least %{count} digits postal code",
    too_long: "must have at most %{count} digits for postal code"}
  validates :province, presence:true ,length: { minimum: 2, maximum: 20, too_short: "must have at least %{count} characters for province",
    too_long: "must have at most %{count} characters for province"}
  validates :barangay, presence:true ,length: { minimum: 2, maximum: 20, too_short: "must have at least %{count} characters for barangay",
    too_long: "must have at most %{count} characters for barangay"}
  #validates :birthday, presence:true
  #validates :guardian_name, length: { minimum: 4, maximum:20, too_short: "must have at least %{count} characters for guardian name",
  #  too_long: "must have at most %{count} characters for guardian name"} #if 2 or more users are related
  #validates :guardian_relationship ,presence:true
  #validates :guardian_contact_number ,presence:true,length: { minimum: 8, maximum:11, too_short: "must have at least %{count} characters for guardian's contact number",
  #  too_long: "must have at most %{count} characters for guardian's contact number"}

  has_many :posts, dependent: :destroy  
 # has_many :notify , class_name:'notifications', :foreign_key=>'subscribed_user'
  has_many :officers, dependent: :destroy 
  has_many :notifications, dependent: :destroy
  has_many :class_lists, dependent: :destroy
  has_many :teacher_lists , dependent: :destroy
  has_many :employee_lists, dependent: :destroy
  #has_many :messages, dependent: :destroy #
  #has_many :thread_messages, dependent: :destroy #
  has_many :group_list, dependent: :destroy
  
  def email_required?
  	false
  end

  def assign_role
    self.role = Role.find_by name: "Regular" if self.role.nil?
  end
  
  def admin?
    if self.role.nil?
      return false
    end
    self.role.name == "Admin"
  end

  def student?
    if self.role.nil?
      return false
    end
    self.role.name == "Students"
  end
  
  def teacher?
    if self.role.nil?
      return false
    end
    self.role.name == "Teacher"
  end

  def employee?
    if self.role.nil?
      return false
    end
    self.role.name == "Employee"
  end

  def name
    "#{first_name} #{middle_name} #{last_name}"
  end

  def self.import(file)
    #r1 = Role.create({name: "Students", description: "By default, student accounts"})
    #r2 = Role.create({name: "Admin", description: "Can perform any CRUD operation on any resource"})
    #r3 = Role.create({name: "Teacher", description: "Can do CRUB operation inside his/her own class"})
    #r4 = Role.create({name: "Employee", description: "Can upload documents for public view"})
    r1=Role.first()
    r2=Role.second()
    r3=Role.third()
    r4=Role.fourth()
    
    success=[]
    #allowed_attributes = ["username", "email", "first_name", "middle_name", "last_name", "phone_number", "suffix","password","valid_till","role_id"]
    allowed_attributes = ["username", "email", "first_name", "middle_name", "last_name", "phone_number", "suffix","password","valid_till","role_id",
"street_address","barangay","postal_code","province","birthday"]
    spreadsheet =  open_spreadsheet(file)
    if spreadsheet != false
      header = spreadsheet.row(1)
      (2..spreadsheet.last_row).each do |i|
          row = Hash[[header, spreadsheet.row(i)].transpose]
          #if user=find_by_username(row["username"])
          if user=find_by_username(row["username"]) #if username exists, do nothing.
          else user=new
            user.attributes = row.to_hash.select { |k,v| allowed_attributes.include? k }
            if user.valid?  #if row satisfies all validation in user  then save
                if row["role_id"]==1
                  user.role_id=r1.id  
                end
                if row["role_id"]==2
                  #user.update_attribute(:role_id=>r2.id)
                  user.role_id=r2.id  
                end
                if row["role_id"]==3
                #  #user.update_attribute(:role_id=>r3.id)
                  user.role_id=r3.id  
                end
                if row["role_id"]==4
                  user.role_id=r4.id  
                #  #user.update_attribute(:role_id=>r4.id)
                end
                  
                user.save!
            else            # else raise error at that row
              #raise "Invalid input at row #{i}."
              #flash[:notice] = "Invalid input at row#{i} with username #{row["username"]} ."
            end #end if else on valid?
          end #end find by username
        end #end loop
    else #
      return false
    end# end if spreadsheet is invalid
  end # end import function

  def self.deletebybatch(file)
    allowed_attributes = ["username", "email", "first_name", "middle_name", "last_name", "phone_number", "suffix","password","valid_till"]
    spreadsheet =  open_spreadsheet(file)
    if spreadsheet != false
      header = spreadsheet.row(1)
      (2..spreadsheet.last_row).each do |i|
          row = Hash[[header, spreadsheet.row(i)].transpose]
          if user=find_by_username(row["username"])
            user.destroy!
          else            # else raise error at that row
             #raise "Invalid input at row #{i}.No such user exists"
          end #end if else
      end # end loop
      #puts "Imported rows #{success}."
    else return false
    end #outer if else
  end # end deletebybatch function

  def self.open_spreadsheet(file)
    
    case File.extname(file.original_filename)
    when ".csv" then Roo::CSV.new(file.path, csv_options: {encoding: "iso-8859-1:utf-8"}  )
    when ".xls" then Roo::Excel.new(file.path, excel_options: {encoding: "iso-8859-1:utf-8"} )
    when ".xlsx" then Roo::Excelx.new(file.path, excelx_options: {encoding: "iso-8859-1:utf-8"} )
    else return false
    end# end case
  end # end function
end
