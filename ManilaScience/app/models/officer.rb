class Officer < ActiveRecord::Base
  belongs_to :user
  belongs_to :class_course
  validates :user_id, presence: true
  validates :class_course_id,presence:true
  validates :position,presence: true,:inclusion => {:in => ['president', 'vice-president','secretary','treasurer']}

end
