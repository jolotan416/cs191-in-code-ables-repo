Rails.application.routes.draw do

  resources :officers
  get 'class_courses/new_club' => 'class_courses#new_club'

  get 'class_lists/new_view' => 'class_lists#new_view'
  post 'class_lists/new_view' => 'class_lists#new_view'

  post 'class_lists/new_class_lists' => 'class_lists#new_class_lists'

  post 'class_view/view_class' => 'class_view#view_class'
  get 'class_view/view_class' => 'class_view#view_class'

  post 'club_view/view_club' => 'club_view#view_club'
  get 'club_view/view_club' => 'club_view#view_club'

  resources :messages
  resources :thread_message_tables
  resources :thread_messages
  get 'notifications/link_through'

  get 'class_courses/new_club' => 'class_courses#new_club'
  get 'notifications/read_post' => 'notifications#read_post'
  post 'notifications/read_post' => 'notifications#read_post'

  get 'posts/new' => 'posts#new'
  post 'posts/new' => 'posts#new'

  get 'posts/:id' => 'posts#show'
  post 'posts/:id' => 'posts#show'

  get 'posts/:id/edit' => 'posts#edit'
  post 'posts/:id/edit' => 'posts#edit'


  resources :announcements
  resources :posts
  resources :messages
  resources :groups
  resources :group_lists
  resources :posts
  resources :calendar_events

  resources :class_lists
  resources :class_courses
  resources :subjects
  resources :works_ins
  resources :departments
  resources :departments
  resources :employee_lists
  resources :teacher_lists
  resources :notifications
  devise_for :views

  devise_for :users, :controllers => {:registrations => "users/registrations", :sessions => "users/sessions"}

  scope "/admin" do
    resources :users
  end

  resources :roles
  resources :users do
    collection { delete :destroy_multiple}
    collection { post :deletebybatch}
    collection { post :import}
    collection do
      delete 'destroy_multiple'
    end
  end

  get 'static/index'

  get 'class_view/index'
  get 'club_view/index'
  get 'group_view/index'
  get 'group_view/edit_student_lists'
  
  post 'group_view/add_user_from_group'
  post 'group_view/remove_user_from_group'


  get 'schedule_view/index'

  get 'club_view' => 'club_view#index'
  get 'class_view' => 'class_view#index'
  get 'group_view' => 'group_view#index'

  post 'group_view/index' => 'group_view#index'

  get 'schedule_view' => 'schedule_view#index'

  post 'calendar_events/new' => 'calendar_events#new'

  #  devise_scope :user do
  #    root to: "devise/sessions#new"
  #  end

  authenticated :admin do
    root :to => 'user#index', as: :authenticated_root
  end

  devise_scope :user do
    root :to => 'devise/sessions#new'
  end



# The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
