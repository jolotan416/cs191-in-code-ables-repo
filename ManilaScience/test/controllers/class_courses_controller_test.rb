require 'test_helper'

class ClassCoursesControllerTest < ActionController::TestCase
  setup do
    @class_course = class_courses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:class_courses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create class_course" do
    assert_difference('ClassCourse.count') do
      post :create, class_course: { room: @class_course.room, schedule: @class_course.schedule, size: @class_course.size, subject_id: @class_course.subject_id, year: @class_course.year }
    end

    assert_redirected_to class_course_path(assigns(:class_course))
  end

  test "should show class_course" do
    get :show, id: @class_course
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @class_course
    assert_response :success
  end

  test "should update class_course" do
    patch :update, id: @class_course, class_course: { room: @class_course.room, schedule: @class_course.schedule, size: @class_course.size, subject_id: @class_course.subject_id, year: @class_course.year }
    assert_redirected_to class_course_path(assigns(:class_course))
  end

  test "should destroy class_course" do
    assert_difference('ClassCourse.count', -1) do
      delete :destroy, id: @class_course
    end

    assert_redirected_to class_courses_path
  end
end
