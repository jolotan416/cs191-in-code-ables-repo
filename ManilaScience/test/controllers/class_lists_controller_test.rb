require 'test_helper'

class ClassListsControllerTest < ActionController::TestCase
  setup do
    @class_list = class_lists(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:class_lists)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create class_list" do
    assert_difference('ClassList.count') do
      post :create, class_list: { class_courses_id: @class_list.class_courses_id, status: @class_list.status, user_id: @class_list.user_id }
    end

    assert_redirected_to class_list_path(assigns(:class_list))
  end

  test "should show class_list" do
    get :show, id: @class_list
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @class_list
    assert_response :success
  end

  test "should update class_list" do
    patch :update, id: @class_list, class_list: { class_courses_id: @class_list.class_courses_id, status: @class_list.status, user_id: @class_list.user_id }
    assert_redirected_to class_list_path(assigns(:class_list))
  end

  test "should destroy class_list" do
    assert_difference('ClassList.count', -1) do
      delete :destroy, id: @class_list
    end

    assert_redirected_to class_lists_path
  end
end
