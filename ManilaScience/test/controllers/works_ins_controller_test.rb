require 'test_helper'

class WorksInsControllerTest < ActionController::TestCase
  setup do
    @works_in = works_ins(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:works_ins)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create works_in" do
    assert_difference('WorksIn.count') do
      post :create, works_in: { deparment_id: @works_in.deparment_id, end_date: @works_in.end_date, start_date: @works_in.start_date, user_id: @works_in.user_id }
    end

    assert_redirected_to works_in_path(assigns(:works_in))
  end

  test "should show works_in" do
    get :show, id: @works_in
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @works_in
    assert_response :success
  end

  test "should update works_in" do
    patch :update, id: @works_in, works_in: { deparment_id: @works_in.deparment_id, end_date: @works_in.end_date, start_date: @works_in.start_date, user_id: @works_in.user_id }
    assert_redirected_to works_in_path(assigns(:works_in))
  end

  test "should destroy works_in" do
    assert_difference('WorksIn.count', -1) do
      delete :destroy, id: @works_in
    end

    assert_redirected_to works_ins_path
  end
end
