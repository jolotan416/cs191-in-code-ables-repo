require 'test_helper'

class TeacherListsControllerTest < ActionController::TestCase
  setup do
    @teacher_list = teacher_lists(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:teacher_lists)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create teacher_list" do
    assert_difference('TeacherList.count') do
      post :create, teacher_list: { title: @teacher_list.title, user_id: @teacher_list.user_id }
    end

    assert_redirected_to teacher_list_path(assigns(:teacher_list))
  end

  test "should show teacher_list" do
    get :show, id: @teacher_list
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @teacher_list
    assert_response :success
  end

  test "should update teacher_list" do
    patch :update, id: @teacher_list, teacher_list: { title: @teacher_list.title, user_id: @teacher_list.user_id }
    assert_redirected_to teacher_list_path(assigns(:teacher_list))
  end

  test "should destroy teacher_list" do
    assert_difference('TeacherList.count', -1) do
      delete :destroy, id: @teacher_list
    end

    assert_redirected_to teacher_lists_path
  end
end
