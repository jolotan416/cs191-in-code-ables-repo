require 'test_helper'

class ThreadMessagesControllerTest < ActionController::TestCase
  setup do
    @thread_message = thread_messages(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:thread_messages)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create thread_message" do
    assert_difference('ThreadMessage.count') do
      post :create, thread_message: { expiry_date: @thread_message.expiry_date, group_id: @thread_message.group_id, string: @thread_message.string, thread_name: @thread_message.thread_name }
    end

    assert_redirected_to thread_message_path(assigns(:thread_message))
  end

  test "should show thread_message" do
    get :show, id: @thread_message
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @thread_message
    assert_response :success
  end

  test "should update thread_message" do
    patch :update, id: @thread_message, thread_message: { expiry_date: @thread_message.expiry_date, group_id: @thread_message.group_id, string: @thread_message.string, thread_name: @thread_message.thread_name }
    assert_redirected_to thread_message_path(assigns(:thread_message))
  end

  test "should destroy thread_message" do
    assert_difference('ThreadMessage.count', -1) do
      delete :destroy, id: @thread_message
    end

    assert_redirected_to thread_messages_path
  end
end
