require 'test_helper'

class ThreadMessageTablesControllerTest < ActionController::TestCase
  setup do
    @thread_message_table = thread_message_tables(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:thread_message_tables)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create thread_message_table" do
    assert_difference('ThreadMessageTable.count') do
      post :create, thread_message_table: { message_id: @thread_message_table.message_id, thread_id: @thread_message_table.thread_id }
    end

    assert_redirected_to thread_message_table_path(assigns(:thread_message_table))
  end

  test "should show thread_message_table" do
    get :show, id: @thread_message_table
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @thread_message_table
    assert_response :success
  end

  test "should update thread_message_table" do
    patch :update, id: @thread_message_table, thread_message_table: { message_id: @thread_message_table.message_id, thread_id: @thread_message_table.thread_id }
    assert_redirected_to thread_message_table_path(assigns(:thread_message_table))
  end

  test "should destroy thread_message_table" do
    assert_difference('ThreadMessageTable.count', -1) do
      delete :destroy, id: @thread_message_table
    end

    assert_redirected_to thread_message_tables_path
  end
end
