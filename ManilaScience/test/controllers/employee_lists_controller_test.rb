require 'test_helper'

class EmployeeListsControllerTest < ActionController::TestCase
  setup do
    @employee_list = employee_lists(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:employee_lists)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create employee_list" do
    assert_difference('EmployeeList.count') do
      post :create, employee_list: { title: @employee_list.title, user_id: @employee_list.user_id }
    end

    assert_redirected_to employee_list_path(assigns(:employee_list))
  end

  test "should show employee_list" do
    get :show, id: @employee_list
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @employee_list
    assert_response :success
  end

  test "should update employee_list" do
    patch :update, id: @employee_list, employee_list: { title: @employee_list.title, user_id: @employee_list.user_id }
    assert_redirected_to employee_list_path(assigns(:employee_list))
  end

  test "should destroy employee_list" do
    assert_difference('EmployeeList.count', -1) do
      delete :destroy, id: @employee_list
    end

    assert_redirected_to employee_lists_path
  end
end
