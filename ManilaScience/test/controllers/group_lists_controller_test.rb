require 'test_helper'

class GroupListsControllerTest < ActionController::TestCase
  setup do
    @group_list = group_lists(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:group_lists)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create group_list" do
    assert_difference('GroupList.count') do
      post :create, group_list: { group_id: @group_list.group_id, user_id: @group_list.user_id }
    end

    assert_redirected_to group_list_path(assigns(:group_list))
  end

  test "should show group_list" do
    get :show, id: @group_list
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @group_list
    assert_response :success
  end

  test "should update group_list" do
    patch :update, id: @group_list, group_list: { group_id: @group_list.group_id, user_id: @group_list.user_id }
    assert_redirected_to group_list_path(assigns(:group_list))
  end

  test "should destroy group_list" do
    assert_difference('GroupList.count', -1) do
      delete :destroy, id: @group_list
    end

    assert_redirected_to group_lists_path
  end
end
