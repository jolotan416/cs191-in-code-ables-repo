class CreateNotifications < ActiveRecord::Migration  
  def change
    create_table :notifications do |t|
      t.string   :user_id
      t.string   :integer
      t.string   :subscribed_user
      t.string   :post_id
      t.string   :class_course
      t.string   identifier
      t.string   :kind
      t.boolean  :read
      t.timestamps null: false
    end
    add_foreign_key :notifications, :users, column: :subscribed_user, dependent: :delete
    add_foreign_key :notifications, :users, column: :user_id, dependent: :delete
    add_foreign_key :notifications, :posts, column: :posr_id, dependent: :delete
  end
end  

      t.references :user_id, index: true, foreign_key: true
      t.references :subscribed_user, index: true, foreign_key: true
      t.references :post_id, index: true, foreign_key: true
      t.references :class_course, index: true, foreign_key: true
      t.integer :identifier