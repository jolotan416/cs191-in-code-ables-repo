# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).

r1 = Role.create({name: "Students", description: "By default, student accounts"})
r2 = Role.create({name: "Admin", description: "Can perform any CRUD operation on any resource"})
r3 = Role.create({name: "Teacher", description: "Can do CRUB operation inside his/her own class"})
r4 = Role.create({name: "Employee", description: "Can upload documents for public view"})

u1 = User.create({username: "samplestudent", email: "cena@example.com", first_name: "John", middle_name: "Michael",
 last_name: "Cena", phone_number: "09152122343", suffix: "", valid_till: DateTime.new(2016,11,12,8,37,48,"+08:00"),
 password: "aaaaaaaa", password_confirmation: "aaaaaaaa", role_id: r1.id,street_address: "mahogany III",barangay: "ginebra",postal_code:"12345",province:"Cagayan",birthday:Date.new(3000,1,1),guardian_name:"Nanny",guardian_relationship:"aunt",guardian_contact_number:"09178911561"})

u2 = User.create({username: "jojobinay", email: "binay@example.com", first_name: "Jojo", middle_name: "Poe",
 last_name: "Binay", phone_number: "09152122343", suffix: "", valid_till: DateTime.new(2016,11,12,8,37,48,"+08:00"),
 password: "aaaaaaaa", password_confirmation: "aaaaaaaa", role_id: r1.id,street_address: "mahogany III",barangay: "ginebra",postal_code:"12345",province:"Cagayan",birthday:Date.new(3000,1,1),guardian_name:"Nanny",guardian_relationship:"aunt",guardian_contact_number:"09178911561"})

u3 = User.create({username: "rodyduterte", email: "duterte@example.com", first_name: "Rodrigo", middle_name: "Defensor",
 last_name: "Duterte", phone_number: "09152122343", suffix: "III", valid_till: DateTime.new(3000,11,12,8,37,48,"+08:00"),
  password: "aaaaaaaa", password_confirmation: "aaaaaaaa", role_id: r1.id,street_address: "mahogany III",barangay: "ginebra",postal_code:"12345",province:"Cagayan",birthday:Date.new(3000,1,1),guardian_name:"Nanny",guardian_relationship:"aunt",guardian_contact_number:"09178911561"})

u4 = User.create({username: "hackereduard", email: "hackereduard@example.com", first_name: "ADMIN", middle_name: "ADMIN",
 last_name: "ADMIN", phone_number: "00000000000", suffix: "", valid_till: DateTime.new(3000,11,12,8,37,48,"+08:00"),
 password: "aaaaaaaa", password_confirmation: "aaaaaaaa", role_id: r2.id,street_address: "mahogany III",barangay: "ginebra",postal_code:"12345",province:"Cagayan",birthday:Date.new(3000,1,1),guardian_name:"Nanny",guardian_relationship:"aunt",guardian_contact_number:"09178911561"})

u5 = User.create({username: "hackerjolo", email: "hackerjolo@example.com", first_name: "ADMIN", middle_name: "ADMIN",
 last_name: "ADMIN", phone_number: "00000000000", suffix: "", valid_till: DateTime.new(2000,11,12,8,37,48,"+08:00"),
 password: "aaaaaaaa", password_confirmation: "aaaaaaaa", role_id: r2.id,street_address: "mahogany III",barangay: "ginebra",postal_code:"12345",province:"Cagayan",birthday:Date.new(3000,1,1),guardian_name:"Nanny",guardian_relationship:"aunt",guardian_contact_number:"09178911561"})

u6 = User.create({username: "sampleadmin", email: "hackertopher@example.com", first_name: "ADMIN", middle_name: "ADMIN",
 last_name: "ADMIN", phone_number: "00000000000", suffix: "I", valid_till: DateTime.new(3000,11,12,8,37,48,"+08:00"),
 password: "aaaaaaaa", password_confirmation: "aaaaaaaa", role_id: r2.id,street_address: "mahogany III",barangay: "ginebra",postal_code:"12345",province:"Cagayan",birthday:Date.new(3000,1,1),guardian_name:"Nanny",guardian_relationship:"aunt",guardian_contact_number:"09178911561"})

u7 = User.create({username: "sampleteacher", email: "hackerteacher@example.com", first_name: "Miriam", middle_name: "Defensor",
 last_name: "Santiago", phone_number: "00000000000", suffix: "I", valid_till: DateTime.new(3000,11,12,8,37,48,"+08:00"),
 password: "aaaaaaaa", password_confirmation: "aaaaaaaa", role_id: r3.id,street_address: "mahogany III",barangay: "ginebra",postal_code:"12345",province:"Cagayan",birthday:Date.new(3000,1,1),guardian_name:"Nanny",guardian_relationship:"aunt",guardian_contact_number:"09178911561"})

u8 = User.create({username: "hackerdimayuga", email: "hackerdiyamuga@example.com", first_name: "Sir John", middle_name: "Dibdib",
 last_name: "Dimayuga", phone_number: "00000000000", suffix: "I", valid_till: DateTime.new(3000,11,12,8,37,48,"+08:00"),
 password: "aaaaaaaa", password_confirmation: "aaaaaaaa", role_id: r3.id,street_address: "mahogany III",barangay: "ginebra",postal_code:"12345",province:"Cagayan",birthday:Date.new(3000,1,1),guardian_name:"Nanny",guardian_relationship:"aunt",guardian_contact_number:"09178911561"})

u9 = User.create({username: "hackeremployee", email: "hackeremployee@example.com", first_name: "Sir", middle_name: "Wilson",
 last_name: "Dragun", phone_number: "00000000000", suffix: "I", valid_till: DateTime.new(3000,11,12,8,37,48,"+08:00"),
 password: "aaaaaaaa", password_confirmation: "aaaaaaaa", role_id: r4.id,street_address: "mahogany III",barangay: "ginebra",postal_code:"12345",province:"Cagayan",birthday:Date.new(3000,1,1),guardian_name:"Nanny",guardian_relationship:"aunt",guardian_contact_number:"09178911561"})

u10 = User.create({username: "hackersusan", email: "hackersusan@example.com", first_name: "Susan", middle_name: "Felizmenio",
 last_name: "Edgardo", phone_number: "00000000000", suffix: "I", valid_till: DateTime.new(3000,11,12,8,37,48,"+08:00"),
 password: "aaaaaaaa", password_confirmation: "aaaaaaaa", role_id: r4.id,street_address: "mahogany III",barangay: "ginebra",postal_code:"12345",province:"Cagayan",birthday:Date.new(3000,1,1),guardian_name:"Nanny",guardian_relationship:"aunt",guardian_contact_number:"09178911561"})


tl1 = TeacherList.create({title: "Instructor I", user_id: u7.id})
tl2 = TeacherList.create({title: "PhD", user_id: u8.id})

el1 = EmployeeList.create({title: "Employee Level I", user_id: u9.id, created_at: Time.now, updated_at: nil})
el2 = EmployeeList.create({title: "Database Admin", user_id: u10.id, created_at: Time.now, updated_at: nil})


d1 = Department.create({name: "Humanities", current_size: 40, capacity: 69})
d2 = Department.create({name: "Mathematics", current_size: 40, capacity: 69})
d3 = Department.create({name: "Physical Education", current_size: 40, capacity: 69})
d4 = Department.create({name: "Music", current_size: 40, capacity: 69})
d5 = Department.create({name: "Computer Science", current_size: 1, capacity: 1})

w1 = WorksIn.create({start_date: Time.now, end_date: nil, department_id: d1.id, user_id: u7.id, created_at: Time.now, updated_at: nil})
w2 = WorksIn.create({start_date: Time.now, end_date: nil, department_id: d2.id, user_id: u8.id, created_at: Time.now, updated_at: nil})
w3 = WorksIn.create({start_date: Time.now, end_date: nil, department_id: d3.id, user_id: u7.id, created_at: Time.now, updated_at: nil})

s1 = Subject.create({subject_name: "Math 17", offered: true, department_id: d2.id, created_at: Time.now, updated_at: Time.now})
s2 = Subject.create({subject_name: "PE 3", offered: true, department_id: d3.id, created_at: Time.now, updated_at: Time.now})
s3 = Subject.create({subject_name: "CS 11", offered: true, department_id: d4.id, created_at: Time.now, updated_at: Time.now})
s4 = Subject.create({subject_name: "CS 192", offered: true, department_id: d4.id, created_at: Time.now, updated_at: Time.now})


cc1 = ClassCourse.create({teacher_id:tl1.user_id,room: "301", year: 2016, schedule: "MWF 11:00am - 12:00nn", size: 1,subject_id: s1.id, created_at: Time.now, updated_at: Time.now, name: "MTHRXWV",is_club: true})
cc2 = ClassCourse.create({teacher_id:tl1.user_id,room: "302", year: 2016, schedule: "MWF 12:00nn - 1:00pm", size: 12, subject_id: s2.id, created_at: Time.now, updated_at: Time.now, name: "MTHRABC",is_club: true})
cc3 = ClassCourse.create({teacher_id:tl2.user_id,room: "303", year: 2016, schedule: "MWF 10:00am - 1:00pm", size: 15, subject_id: s3.id, created_at: Time.now, updated_at: Time.now, name: "MTHRXYZ",is_club: true})
cc4 = ClassCourse.create({teacher_id:tl2.user_id,room: "304", year: 2016, schedule: "TTh 1:00pm - 8:00pm", size: 15, subject_id: s4.id, created_at: Time.now, updated_at: Time.now, name: "THU",is_club: true})


cl1 = ClassList.create({user_id: u1.id, class_course_id: cc1.id, status: "enrolled", created_at: Time.now, updated_at: Time.now})
cl2 = ClassList.create({user_id: u2.id, class_course_id: cc1.id, status: "enrolled", created_at: Time.now, updated_at: Time.now})
cl3 = ClassList.create({user_id: u3.id, class_course_id: cc1.id, status: "enrolled", created_at: Time.now, updated_at: Time.now})
cl4 = ClassList.create({user_id: u1.id, class_course_id: cc2.id, status: "enrolled", created_at: Time.now, updated_at: Time.now})
cl5 = ClassList.create({user_id: u2.id, class_course_id: cc2.id, status: "enrolled", created_at: Time.now, updated_at: Time.now})
cl6 = ClassList.create({user_id: u1.id, class_course_id: cc3.id, status: "enrolled", created_at: Time.now, updated_at: Time.now})

g1 = Group.create({group_name: "Team 1", group_description: "Potchi naman oh", class_id: cc1.id})
g2 = Group.create({group_name: "Team Guumy Worms", group_description: "Gummy", class_id: cc2.id})
g3 = Group.create({group_name: "Team Wilson", group_description: "Adaaaa", class_id: cc3.id})
g4 = Group.create({group_name: "Team Iya", group_description: "Iyaaaa", class_id: cc4.id})
g5 = Group.create({group_name: "Team Poe", group_description: "Potchi naman oh", class_id: cc1.id })
g6 = Group.create({group_name: "Team Guumy Bear", group_description: "Gummy", class_id: cc2.id})
g7 = Group.create({group_name: "Team Binay", group_description: "Jejemon", class_id: cc3.id})
g8 = Group.create({group_name: "Team Duterte", group_description: "Patay tayo diyan brad", class_id: cc4.id})
g9=Group.create(group_name:"3moteRaHgurls",group_description:"Stat101 group",class_id:cc1.id)
tm1=ThreadMessage.create(thread_name:"Lason-an sa Forever Summer",string:"beat dropped harder than my grades.",group_id:g1.id) 	#expiry_date can't be blank?
m1=Message.create(author_id:u1.id,content:"OMG!")
m2=Message.create(author_id:u1.id,content:"IKR!")

tmt1=ThreadMessageTable.create(thread_id:tm1.id,message_id:m1.id)
tmt2=ThreadMessageTable.create(thread_id:tm1.id,message_id:m1.id)
#tmt3=ThreadMessageTable.create(thread_id:tm1.id,message_id:m3.id)
# g1 = Group.create({group_name: "Team Potchi", group_description: "Potchi naman oh"})
# g2 = Group.create({9group_name: "Tropang Guumy Worms", group_description: "Gummy"})
# g3 = Group.create({group_name: "Tropang Ada", group_description: "Adaaaa"})
# g4 = Group.create({group_name: "Tropang Iya", group_description: "Iyaaaa"})



# rails g scaffold Message author_id:integer content:string flag:boolean	 # Messages
# rails g scaffold ThreadMessageTable thread_id:integer message_id:integer # Thread has many messages
# rails g scaffold Thread thread_name: string group_id:integer expiry_date:datetime			 # Primary key nito ay thread_id
# rails g scaffold Group group_name:string group_description:string class_id:integer
# rails g scaffold GroupList user_id:integer group_id:integer
