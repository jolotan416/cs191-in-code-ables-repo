class CreateThreadMessageTables < ActiveRecord::Migration
  def change
    create_table :thread_message_tables do |t|
      t.integer :thread_id
      t.integer :message_id

      t.timestamps null: false
    end
  end
end
