class CreateAnnouncements < ActiveRecord::Migration
  def change
    create_table :announcements do |t|
      t.string :title
      t.string :body
      t.datetime :edit_time
      t.boolean :visibility
      t.integer :author_id

      t.timestamps null: false
    end
  end
end
