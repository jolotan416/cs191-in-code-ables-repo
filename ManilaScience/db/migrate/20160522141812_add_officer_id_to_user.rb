class AddOfficerIdToUser < ActiveRecord::Migration
  def change
    add_reference :users, :officer, index: true, foreign_key: true
  end
end
