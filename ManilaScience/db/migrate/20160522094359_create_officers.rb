class CreateOfficers < ActiveRecord::Migration
  def change
    create_table :officers do |t|
      t.references :user
      t.string :position
      t.references :class_course
      t.datetime :from
      t.datetime :to
      t.string :assigned_by

      t.timestamps null: false
    end
  end
end
