class AddNameToClassCourses < ActiveRecord::Migration
  def change
    add_column :class_courses, :name, :string
  end
end
