class CreateThreadMessages < ActiveRecord::Migration
  def change
    create_table :thread_messages do |t|
      t.string :thread_name
      t.string :string
      t.integer :group_id
      t.datetime :expiry_date

      t.timestamps null: false
    end
  end
end
