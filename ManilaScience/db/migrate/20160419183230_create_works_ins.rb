class CreateWorksIns < ActiveRecord::Migration
  def change
    create_table :works_ins do |t|
      t.datetime :start_date
      t.datetime :end_date
      t.references :department, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
