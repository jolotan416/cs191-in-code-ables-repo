class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.string :group_name
      t.string :group_description
      t.integer :class_id

      t.timestamps null: false
    end
  end
end
