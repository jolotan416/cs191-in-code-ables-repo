class AddClassCourseToPosts < ActiveRecord::Migration
  def change
    add_reference :posts, :class_course, index: true, foreign_key: true
  end
end
