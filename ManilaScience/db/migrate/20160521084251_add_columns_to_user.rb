class AddColumnsToUser < ActiveRecord::Migration
  def change
    add_column :users, :street_address, :string
    add_column :users, :barangay, :string
    add_column :users, :postal_code, :string
    add_column :users, :province, :string
    add_column :users, :birthday, :date
    add_column :users, :guardian_name, :string
    add_column :users, :guardian_relationship, :string
    add_column :users, :guardian_contact_number, :string
  end
end
