class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.integer :author_id
      t.string :content
      t.boolean :flag

      t.timestamps null: false
    end
  end
end
