class AddTeacherToClassCourse < ActiveRecord::Migration
  def change
    add_column :class_courses, :teacher_id, :int
  end
end
