class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      #t.integer :subscribed_user
      t.references :user
      t.references :subscribed_user, references: :users
      t.references :post
      t.references :class_course
      t.integer :identifier
      t.string :type
      t.boolean :read

      t.timestamps null: false
    end
    #add_foreign_key :notifications, :users, column: :user_id
    #add_foreign_key :notifications, :users, column: :subscribed_user
    #add_foreign_key :notifications, :posts, column: :post_id
    #add_foreign_key :notifications, :class_courses, column: :class_course
end
end