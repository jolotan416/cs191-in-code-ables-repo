class AddIsClubToClassCourse < ActiveRecord::Migration
  def change
    add_column :class_courses, :is_club, :boolean
  end
end
