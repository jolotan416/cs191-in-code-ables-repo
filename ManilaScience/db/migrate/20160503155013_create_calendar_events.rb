class CreateCalendarEvents < ActiveRecord::Migration
  def change
    create_table :calendar_events do |t|
      t.string :title
      t.string :event_type
      t.datetime :start_time
      t.datetime :end_time
      t.string :description
      t.timestamps null: false

    end
  end
end
