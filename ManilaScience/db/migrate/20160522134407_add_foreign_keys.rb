class AddForeignKeys < ActiveRecord::Migration
  def change
    add_foreign_key "notifications", "class_courses", name: "notifications_class_course_id_fk"
    add_foreign_key "notifications", "posts", name: "notifications_post_id_fk"
    add_foreign_key "notifications", "users", name: "notifications_user_id_fk"
 
  end
end
