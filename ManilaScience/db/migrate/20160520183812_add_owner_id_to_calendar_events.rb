class AddOwnerIdToCalendarEvents < ActiveRecord::Migration
  def change
    add_column :calendar_events, :owner_id, :integer
  end
end
