# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160523010120) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "announcements", force: :cascade do |t|
    t.string   "title"
    t.string   "body"
    t.datetime "edit_time"
    t.boolean  "visibility"
    t.integer  "author_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "calendar_events", force: :cascade do |t|
    t.string   "title"
    t.string   "event_type"
    t.datetime "start_time"
    t.datetime "end_time"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "owner_id"
  end

  create_table "class_courses", force: :cascade do |t|
    t.string   "room"
    t.integer  "year"
    t.string   "schedule"
    t.integer  "size"
    t.integer  "subject_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "name"
    t.integer  "teacher_id"
    t.boolean  "is_club"
  end

  add_index "class_courses", ["subject_id"], name: "index_class_courses_on_subject_id", using: :btree

  create_table "class_lists", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "class_course_id"
    t.string   "status"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "class_lists", ["class_course_id"], name: "index_class_lists_on_class_course_id", using: :btree
  add_index "class_lists", ["user_id"], name: "index_class_lists_on_user_id", using: :btree

  create_table "departments", force: :cascade do |t|
    t.string   "name"
    t.integer  "current_size"
    t.integer  "capacity"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "employee_lists", force: :cascade do |t|
    t.string   "title"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "employee_lists", ["user_id"], name: "index_employee_lists_on_user_id", using: :btree

  create_table "group_lists", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "group_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "groups", force: :cascade do |t|
    t.string   "group_name"
    t.string   "group_description"
    t.integer  "class_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "messages", force: :cascade do |t|
    t.integer  "author_id"
    t.string   "content"
    t.boolean  "flag"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "thread_id"
  end

  create_table "notifications", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "subscribed_user_id"
    t.integer  "post_id"
    t.integer  "class_course_id"
    t.integer  "identifier"
    t.string   "type"
    t.boolean  "read"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "announcement_id"
  end

  create_table "officers", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "position"
    t.integer  "class_course_id"
    t.datetime "from"
    t.datetime "to"
    t.string   "assigned_by"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "officers", ["class_course_id"], name: "index_officers_on_class_course_id", using: :btree
  add_index "officers", ["user_id"], name: "index_officers_on_user_id", using: :btree

  create_table "posts", force: :cascade do |t|
    t.string   "caption"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.integer  "user_id"
    t.integer  "class_course_id"
    t.string   "document_file_name"
    t.string   "document_content_type"
    t.integer  "document_file_size"
    t.datetime "document_updated_at"
  end

  add_index "posts", ["class_course_id"], name: "index_posts_on_class_course_id", using: :btree
  add_index "posts", ["user_id"], name: "index_posts_on_user_id", using: :btree

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "subjects", force: :cascade do |t|
    t.string   "subject_name"
    t.boolean  "offered"
    t.integer  "department_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "subjects", ["department_id"], name: "index_subjects_on_department_id", using: :btree

  create_table "teacher_lists", force: :cascade do |t|
    t.string   "title"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "teacher_lists", ["user_id"], name: "index_teacher_lists_on_user_id", using: :btree

  create_table "thread_message_tables", force: :cascade do |t|
    t.integer  "thread_id"
    t.integer  "message_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "thread_messages", force: :cascade do |t|
    t.string   "thread_name"
    t.string   "string"
    t.integer  "group_id"
    t.datetime "expiry_date"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "username"
    t.string   "email",                   default: "", null: false
    t.string   "first_name"
    t.string   "middle_name"
    t.string   "last_name"
    t.string   "phone_number"
    t.string   "suffix"
    t.datetime "valid_till"
    t.integer  "role_id"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.string   "encrypted_password",      default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",           default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "street_address"
    t.string   "barangay"
    t.string   "postal_code"
    t.string   "province"
    t.date     "birthday"
    t.string   "guardian_name"
    t.string   "guardian_relationship"
    t.string   "guardian_contact_number"
    t.integer  "officer_id"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["officer_id"], name: "index_users_on_officer_id", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["role_id"], name: "index_users_on_role_id", using: :btree

  create_table "views", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "views", ["email"], name: "index_views_on_email", unique: true, using: :btree
  add_index "views", ["reset_password_token"], name: "index_views_on_reset_password_token", unique: true, using: :btree

  create_table "works_ins", force: :cascade do |t|
    t.datetime "start_date"
    t.datetime "end_date"
    t.integer  "department_id"
    t.integer  "user_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "works_ins", ["department_id"], name: "index_works_ins_on_department_id", using: :btree
  add_index "works_ins", ["user_id"], name: "index_works_ins_on_user_id", using: :btree

  add_foreign_key "class_courses", "subjects"
  add_foreign_key "class_lists", "class_courses"
  add_foreign_key "class_lists", "users"
  add_foreign_key "employee_lists", "users"
  add_foreign_key "group_lists", "users", name: "group_lists_user_id_fk"
  add_foreign_key "notifications", "announcements"
  add_foreign_key "notifications", "class_courses", name: "notifications_class_course_id_fk"
  add_foreign_key "notifications", "posts", name: "notifications_post_id_fk"
  add_foreign_key "notifications", "users", name: "notifications_user_id_fk"
  add_foreign_key "officers", "class_courses"
  add_foreign_key "officers", "users"
  add_foreign_key "posts", "class_courses", name: "posts_class_course_id_fk"
  add_foreign_key "posts", "users", name: "posts_user_id_fk"
  add_foreign_key "subjects", "departments", name: "subjects_department_id_fk"
  add_foreign_key "teacher_lists", "users", name: "teacher_lists_user_id_fk"
  add_foreign_key "users", "officers"
  add_foreign_key "users", "roles", name: "users_role_id_fk"
  add_foreign_key "works_ins", "users", name: "works_ins_user_id_fk"
end
